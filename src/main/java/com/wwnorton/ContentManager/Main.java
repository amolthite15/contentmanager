package com.wwnorton.ContentManager;

import java.io.File;

import com.wwnorton.ContentManager.util.HibernateUtil;

public class Main {

	public static void main(String[] args) {
		System.out.println("Arguments Count = " + args.length);
		if (null != args && args.length > 0) {
			Integer sourceBookId = Integer.parseInt(args[0]);
			Integer targetBookId = Integer.parseInt(args[1]);
			if (null != sourceBookId && null != targetBookId) {
				try {
					Main main = new Main();
					File file = main.getDatabaseConfigurationFile();
					HibernateUtil.loadDatabaseConfiguration(file);
				} catch (Exception e) {
					System.out.println("--- Failed to load DB configuration " + e);
					System.exit(0);
				}

				System.out.println("Source Book Id = " + sourceBookId + ",   Target Book Id = " + targetBookId);

				ContentHandler handler = new ContentHandler();
				handler.copyQuestionContent(sourceBookId, targetBookId);
				try {
					System.out.println("Releasing resources.....!!!!");
					Thread.sleep(2000);
					HibernateUtil.closeSessionFactory();
				} catch (Exception e) {
					System.err.println("Error : " + e);
				}
			} else {
				System.err.println("Source book id or Target book id should not be null.");
			}
			System.exit(0);
		} else {
			System.err.println("Source book id and Target book id need to be passed as a command line argument to process the copy operation.");
		}

	}

	private File getDatabaseConfigurationFile() {
		String projectDirectory = System.getProperty("user.dir");
		String fileName = projectDirectory + "/db.properties";

		return new File(fileName);
	}

}
