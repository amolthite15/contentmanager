package com.wwnorton.ContentManager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "ps_components")
@Proxy(lazy = false)
@JsonIgnoreProperties(value = {"part"})
public class Components implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 7739387243881386542L;

  @XmlElement
  private String componentID;

  private Part part;

  @XmlElement
  private Integer componentType;

  @XmlElement
  private String componentDatas;

  @XmlElement
  private String answerKey;

  @XmlElement
  private String componentDesign;

  @XmlElement
  private Integer componentLevel = 1;

  @XmlElement
  // private Double gradeWeight = 0.0;

  /**
   * @return the componentID
   */
  @Id
  @Column(name = "Component_ID", unique = true, nullable = false)
  public String getComponentID() {
    return componentID;
  }

  /**
   * @param componentID the componentID to set
   */
  public void setComponentID(String componentID) {
    this.componentID = componentID;
  }

  /**
   * @return the part
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "part_id", nullable = false)
  public Part getPart() {
    return part;
  }

  /**
   * @param part the part to set
   */
  public void setPart(Part part) {
    this.part = part;
  }

  /**
   * @return the componentType
   */
  @Column(name = "Component_Type")
  public Integer getComponentType() {
    return componentType;
  }

  /**
   * @See the componentsType to set .
   * @param componentType .
   */
  public void setComponentType(Integer componentType) {
    this.componentType = componentType;
  }

  /**
   * 
   * @return componentData .
   */
  @Column(name = "Component_Data")
  public String getComponentDatas() {
    return componentDatas;
  }

  /**
   * 
   * @param componentData .
   */
  public void setComponentDatas(String componentData) {
    this.componentDatas = componentData;
  }

  /**
   * 
   * @return answerKey.
   */
  @Column(name = "Answer_Key")
  public String getAnswerKey() {
    return answerKey;
  }

  /**
   * 
   * @param answerKey .
   */
  public void setAnswerKey(String answerKey) {
    this.answerKey = answerKey;
  }

  @Column(name = "Component_Design")
  public String getComponentDesign() {
    return componentDesign;
  }

  public void setComponentDesign(String componentDesign) {
    this.componentDesign = componentDesign;
  }

  @Column(name = "Component_Level")
  public Integer getComponentLevel() {
    return componentLevel;
  }

  public void setComponentLevel(Integer componentLevel) {
    this.componentLevel = componentLevel;
  }



  // /**
  // * @return the gradeWeight
  // */
  // @Column(name="Grade")
  // public Double getGradeWeight() {
  // return gradeWeight;
  // }
  //
  // /**
  // * @param gradeWeight the gradeWeight to set
  // */
  // public void setGradeWeight(Double gradeWeight) {
  // this.gradeWeight = gradeWeight;
  // }
}