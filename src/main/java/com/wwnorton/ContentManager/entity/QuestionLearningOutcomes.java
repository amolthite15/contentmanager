package com.wwnorton.ContentManager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_question_learning_outcomes")
public class QuestionLearningOutcomes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5780407762956330565L;
	/**
	 * 
	 */
	private Integer questionOutcomesId;
	/**
	 * 
	 */
	private Integer questionBookId;
	/**
	 * 
	 */
	private Integer learningOutcomeId;
	/**
	 * 
	 */
	private Integer bookId;
	/**
	 * 
	 */
	private Integer questionId;
	/**
	 * 
	 */
	private Integer learningOutComeSetId;

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Question_Outcomes_Id", unique = true, nullable = false)
	public Integer getQuestionOutcomesId() {
		return questionOutcomesId;
	}

	public void setQuestionOutcomesId(Integer questionOutcomesId) {
		this.questionOutcomesId = questionOutcomesId;
	}

	@Column(name = "Question_Book_Id")
	public Integer getQuestionBookId() {
		return questionBookId;
	}

	public void setQuestionBookId(Integer questionBookId) {
		this.questionBookId = questionBookId;
	}

	@Column(name = "Learning_Outcome_Id")
	public Integer getLearningOutcomeId() {
		return learningOutcomeId;
	}

	public void setLearningOutcomeId(Integer learningOutcomeId) {
		this.learningOutcomeId = learningOutcomeId;
	}

	@Column(name = "Book_Id")
	public Integer getBookId() {
		return bookId;
	}

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	@Column(name = "Question_Id")
	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Column(name = "Learning_Outcome_Set_Id")
	public Integer getLearningOutComeSetId() {
		return learningOutComeSetId;
	}

	public void setLearningOutComeSetId(Integer learningOutComeSetId) {
		this.learningOutComeSetId = learningOutComeSetId;
	}

}
