package com.wwnorton.ContentManager.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "ps_part")
@Proxy(lazy = false)
@JsonIgnoreProperties(value = { "question" })
public class Part implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5728196950877263691L;

	/**
	 * 
	 */
	@XmlElement
	private Integer partID;

	@XmlElement
	private String text;

	@XmlElement
	private String content;

	private QuestionForRest question;

	@XmlElement
	private String solutionText;

	@XmlElement
	private String hintText;

	@XmlElement
	private List<Components> components;

	@XmlElement
	private Double gradeWeight = 0.0;

	@XmlElement
	private String gradeTipText;

	@XmlElement
	private int gradeTipTextSwitch;

	/**
	 * @param partID
	 *            .
	 */
	public void setPartID(Integer partID) {
		this.partID = partID;
	}

	/**
	 * 
	 * @return questionID
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "question_id", nullable = false)
	public QuestionForRest getQuestion() {
		return question;
	}

	/**
	 * 
	 * @param question
	 *            .
	 */
	public void setQuestion(QuestionForRest question) {
		this.question = question;
	}

	/**
	 * 
	 * @return partID
	 */
	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Part_ID", unique = true, nullable = false)
	public Integer getPartID() {
		return partID;
	}

	/**
	 * @return components
	 */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "part", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	public List<Components> getComponents() {
		return components;
	}

	/**
	 * 
	 * @param components
	 *            .
	 */
	public void setComponents(List<Components> components) {
		this.components = components;
	}

	/**
	 * @return the solutionText
	 */
	@Column(name = "Part_Solution")
	public String getSolutionText() {
		return solutionText;
	}

	/**
	 * @param solutionText
	 *            the solutionText to set
	 */
	public void setSolutionText(String solutionText) {
		this.solutionText = solutionText;
	}

	/**
	 * @return the hintText
	 */
	@Column(name = "Part_Hint")
	public String getHintText() {
		return hintText;
	}

	/**
	 * @param hintText
	 *            the hintText to set
	 */
	public void setHintText(String hintText) {
		this.hintText = hintText;
	}

	/**
	 * 
	 * @return text.
	 */
	@Column(name = "Part_Text")
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param text
	 *            .
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * 
	 * @return .
	 */
	@Column(name = "Part_Content")
	public String getContent() {
		return content;
	}

	/**
	 * 
	 * @param content
	 *            .
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the gradeWeight
	 */
	@Column(name = "Part_Grade")
	public Double getGradeWeight() {
		return gradeWeight;
	}

	/**
	 * @param gradeWeight
	 *            the gradeWeight to set
	 */
	public void setGradeWeight(Double gradeWeight) {
		this.gradeWeight = gradeWeight;
	}

	/**
	 * @return the Part_Grade_Tip
	 */
	@Column(name = "Part_Grade_Tip")
	public String getGradeTipText() {
		return gradeTipText;
	}

	/**
	 * @param Part_Grade_Tip
	 *            the Part_Grade_Tip to set
	 */
	public void setGradeTipText(String gradeTipText) {
		this.gradeTipText = gradeTipText;
	}

	/**
	 * @return the Part_Grade_Tip
	 */
	@Column(name = "Part_Grade_Tip_Switch")
	public int getGradeTipTextSwitch() {
		return gradeTipTextSwitch;
	}

	/**
	 * @param Part_Grade_Tip
	 *            the Part_Grade_Tip to set checked
	 */
	public void setGradeTipTextSwitch(int gradeTipTextSwitch) {
		this.gradeTipTextSwitch = gradeTipTextSwitch;
	}
}