package com.wwnorton.ContentManager.entity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_graph")
public class Graph implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5289645201092064490L;

	private Integer graphID;

	private int questionID;

	private String graph_img_ID;

	private String graph_name;

	private String author;

	private String graph_data;

	private String imageData;

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "graph_ID", unique = true, nullable = false)
	public Integer getGraphID() {
		return graphID;
	}

	public void setGraphID(Integer graphID) {
		this.graphID = graphID;
	}

	@Column(name = "Question_ID")
	public int getQuestionID() {
		return questionID;
	}

	public void setQuestionID(int questionID) {
		this.questionID = questionID;
	}

	@Column(name = "Graph_Img_ID", unique = true, nullable = false)
	public String getGraph_img_ID() {
		return graph_img_ID;
	}

	public void setGraph_img_ID(String graphImgID) {
		graph_img_ID = graphImgID;
	}

	@Column(name = "Graph_Name")
	public String getGraph_name() {
		return graph_name;
	}

	public void setGraph_name(String graphName) {
		graph_name = graphName;
	}

	@Column(name = "Author")
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Column(name = "Graph_Data")
	public String getGraph_data() {
		return graph_data;
	}

	public void setGraph_data(String graphData) {
		graph_data = graphData;
	}

	@Column(name = "ImageData")
	public String getImageData() {
		return imageData;
	}

	public void setImageData(String imageData) {
		this.imageData = imageData;
	}

	public Object deepCopy() throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(this);
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bis);
		return ois.readObject();
	}

}
