package com.wwnorton.ContentManager.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_book")
public class Book implements Serializable {

	private static final long serialVersionUID = 458472700313231214L;

	private int bookID;

	private String bookName;

	private String masterBookNumber;

	private String code;

	private List<Chapter> chapters;

	private String vbID;

	private Integer isSignificant;

	private Integer displayPeriodic = 1;

	private String graphId;

	private Integer commaSeparate = 0;

	private Integer enabledLos = 0;

	private String nciaProductCode;

	public Book() {
		super();
	}

	public Book(int bookID, String bookName, Integer displayPeriodic, String graphId) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
		this.displayPeriodic = displayPeriodic;
		this.graphId = graphId;
	}

	/**
	 * 
	 * @param bookID
	 * @param bookName
	 */
	public Book(int bookID, String bookName) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
	}

	/**
	 * 
	 * @param bookID
	 * @param bookName
	 * @param code
	 */
	public Book(int bookID, String bookName, String code) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
		this.code = code;
		this.nciaProductCode = code;
	}

	/**
	 * 
	 * @param bookID
	 * @param bookName
	 * @param code
	 */
	public Book(int bookID, String bookName, String code, Integer isSignificant) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
		this.code = code;
		this.isSignificant = isSignificant;
		this.nciaProductCode = code;
	}

	/**
	 * 
	 * @param bookID
	 * @param bookName
	 * @param code
	 * @param isSignificant
	 * @param commaSeparate
	 */
	public Book(int bookID, String bookName, String code, Integer isSignificant, Integer commaSeparate) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
		this.code = code;
		this.isSignificant = isSignificant;
		this.commaSeparate = commaSeparate;
		this.nciaProductCode = code;
	}

	/**
	 * 
	 * @param bookID
	 * @param bookName
	 * @param code
	 */
	public Book(int bookID, String bookName, String code, Integer isSignificant, Integer commaSeparate,
			Integer enabledLos) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
		this.code = code;
		this.isSignificant = isSignificant;
		this.commaSeparate = commaSeparate;
		this.enabledLos = enabledLos;
		this.nciaProductCode = code;
	}

	/**
	 * 
	 * @param bookID
	 * @param bookName
	 * @param chapters
	 */
	public Book(int bookID, String bookName, List<Chapter> chapters) {
		super();
		this.bookID = bookID;
		this.bookName = bookName;
		this.chapters = chapters;
	}

	/**
	 * @return the bookID
	 */
	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Book_ID", unique = true, nullable = false)
	public int getBookID() {
		return bookID;
	}

	/**
	 * @param bookID
	 *            the bookID to set
	 */
	public void setBookID(int bookID) {
		this.bookID = bookID;
	}

	/**
	 * @return the bookName
	 */
	@Column(name = "Book_Name")
	public String getBookName() {
		return bookName;
	}

	/**
	 * @param bookName
	 *            the bookName to set
	 */
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	@Column(name = "Master_Book_Number")
	public String getMasterBookNumber() {
		return masterBookNumber;
	}

	public void setMasterBookNumber(String masterBookNumber) {
		this.masterBookNumber = masterBookNumber;
	}

	@Column(name = "Code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the chapters
	 */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "book", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	public List<Chapter> getChapters() {
		return chapters;
	}

	/**
	 * @param chapters
	 *            the chapters to set
	 */
	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}

	@Column(name = "VBID")
	public String getVbID() {
		return vbID;
	}

	public void setVbID(String vbID) {
		this.vbID = vbID;
	}

	@Column(name = "Significant_Figures", nullable = false, columnDefinition = "int default 1")
	public Integer getIsSignificant() {
		return isSignificant;
	}

	public void setIsSignificant(Integer isSignificant) {
		this.isSignificant = isSignificant;
	}

	@Column(name = "Display_Periodic", nullable = false, columnDefinition = "int default 0")
	public Integer getDisplayPeriodic() {
		return displayPeriodic;
	}

	public void setDisplayPeriodic(Integer displayPeriodic) {
		this.displayPeriodic = displayPeriodic;
	}

	@Column(name = "Graph_ID")
	public String getGraphId() {
		return graphId;
	}

	public void setGraphId(String graphId) {
		this.graphId = graphId;
	}

	@Column(name = "Comma_Separate")
	public Integer getCommaSeparate() {
		return commaSeparate;
	}

	public void setCommaSeparate(Integer commaSeparate) {
		this.commaSeparate = commaSeparate;
	}

	@Column(name = "enabled_los")
	public Integer getEnabledLos() {
		return enabledLos;
	}

	public void setEnabledLos(Integer enabledLos) {
		this.enabledLos = enabledLos;
	}

	@Column(name = "NCIA_Product_Code")
	public String getNciaProductCode() {
		return nciaProductCode;
	}

	public void setNciaProductCode(String nciaProductCode) {
		this.nciaProductCode = nciaProductCode;
	}
}