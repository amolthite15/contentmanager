package com.wwnorton.ContentManager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_book_author")
public class BookAuthor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer bookAuthorID;

	private Book book;

	private String authorID;

	private String assigner;

	/**
	 * @return bookAuthorID
	 */
	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Book_Author_ID", unique = true, nullable = false)
	public Integer getBookAuthorID() {
		return bookAuthorID;
	}

	/**
	 * Set book author id.
	 * 
	 * @param bookAuthorID
	 */
	public void setBookAuthorID(Integer bookAuthorID) {
		this.bookAuthorID = bookAuthorID;
	}

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "Book_ID", nullable = false)
	public Book getBook() {
		return book;
	}

	public void setBook(Book bookID) {
		this.book = bookID;
	}

	@Column(name = "Author_ID")
	public String getAuthorID() {
		return authorID;
	}

	/**
	 * set author id
	 * 
	 * @param authorID
	 */
	public void setAuthorID(String authorID) {
		this.authorID = authorID;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "Assigner")
	public String getAssigner() {
		return assigner;
	}

	public void setAssigner(String assigner) {
		this.assigner = assigner;
	}

}
