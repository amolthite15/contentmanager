package com.wwnorton.ContentManager.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "PS_Pool_Question")
public class PoolQuestion implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Pool QuestionId
	 */

	private int poolQuestionId;

	/**
	 * pool ID
	 */

	private int poolId;

	/**
	 * Question ID
	 */

	private int questionId;

	/**
	 * QuestionIndex
	 */

	private int questionIndex;

	private Date createTime;

	private Date updateTime;

	public PoolQuestion() {
	}

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Pool_Question_ID", unique = true, nullable = false)
	public int getPoolQuestionId() {
		return poolQuestionId;
	}

	public void setPoolQuestionId(int poolQuestionId) {
		this.poolQuestionId = poolQuestionId;
	}

	@Column(name = "Pool_ID")
	public int getPoolId() {
		return poolId;
	}

	public void setPoolId(int poolId) {
		this.poolId = poolId;
	}

	@Column(name = "Question_ID")
	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	@Column(name = "Question_Index")
	public int getQuestionIndex() {
		return questionIndex;
	}

	public void setQuestionIndex(int questionIndex) {
		this.questionIndex = questionIndex;
	}

	@Column(name = "Create_Time", updatable = false)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "Update_Time")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
