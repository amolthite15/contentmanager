package com.wwnorton.ContentManager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "PS_Parameter")
@JsonIgnoreProperties(value = { "question" }) // 解决Java对象转换成JSON时的递归问题
public class Parameter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4944130465430016589L;

	private Integer parameterID;

	private String paramID;

	private Question question;

	private String paramName;

	private int paramType;

	private int paramSubType;

	private String paramData;

	private int dependOn;

	private String dependOnParamID;

	/**
	 * @return the parameterID
	 */
	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getParameterID() {
		return parameterID;
	}

	/**
	 * @param parameterID
	 *            the parameterID to set
	 */
	public void setParameterID(Integer parameterID) {
		this.parameterID = parameterID;
	}

	/**
	 * @return the paramID
	 */

	@Column(name = "Param_ID", unique = true, nullable = false)
	public String getParamID() {
		return paramID;
	}

	/**
	 * @param paramID
	 *            the paramID to set
	 */
	public void setParamID(String paramID) {
		this.paramID = paramID;
	}

	/**
	 * @return the question
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Question_ID", nullable = false)
	public Question getQuestion() {
		return question;
	}

	/**
	 * @param question
	 *            the question to set
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}

	/**
	 * @return the paramName
	 */
	@Column(name = "Param_Name")
	public String getParamName() {
		return paramName;
	}

	/**
	 * @param paramName
	 *            the paramName to set
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	/**
	 * @return the paramType
	 */
	@Column(name = "Param_Type")
	public int getParamType() {
		return paramType;
	}

	/**
	 * @param paramType
	 *            the paramType to set
	 */
	public void setParamType(int paramType) {
		this.paramType = paramType;
	}

	/**
	 * @return the paramData
	 */
	@Column(name = "Param_Data")
	public String getParamData() {
		return paramData;
	}

	/**
	 * @param paramData
	 *            the paramData to set
	 */
	public void setParamData(String paramData) {
		this.paramData = paramData;
	}

	/**
	 * 
	 * @return paramSubType.
	 */
	@Column(name = "Param_Sub_Type")
	public int getParamSubType() {
		return paramSubType;
	}

	/**
	 * 
	 * @param paramSubType
	 *            .
	 */
	public void setParamSubType(int paramSubType) {
		this.paramSubType = paramSubType;
	}

	/**
	 * 
	 * @return dependOn.
	 */
	@Column(name = "Is_Depend_On")
	public int getDependOn() {
		return dependOn;
	}

	/**
	 * 
	 * @param dependOn
	 *            .
	 */
	public void setDependOn(int dependOn) {
		this.dependOn = dependOn;
	}

	/**
	 * 
	 * @return dependOnParamID.
	 */
	@Column(name = "Depend_On_Param_ID")
	public String getDependOnParamID() {
		return dependOnParamID;
	}

	/**
	 * 
	 * @param dependOnParamID
	 *            .
	 */
	public void setDependOnParamID(String dependOnParamID) {
		this.dependOnParamID = dependOnParamID;
	}

}
