package com.wwnorton.ContentManager.entity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Proxy;

@Entity
@Proxy(lazy = false)
@DiscriminatorValue("P")
public class QuestionForRest extends Question {

	public QuestionForRest() {

	}

	public QuestionForRest(Integer questionID, String questionTitle, Integer questionState, Double grade) {
		super(questionID, questionTitle, questionState, grade);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2359283084325075274L;

	private List<Part> parts;

	private List<Parameter> parameters;

	/**
	 * @return the parts
	 */

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "question", fetch = FetchType.EAGER)
	@OrderBy(value = "partID ASC")
	@Fetch(value = FetchMode.SELECT)
	public List<Part> getParts() {
		return parts;
	}

	/**
	 * @param parts
	 *            the parts to set
	 */
	public void setParts(List<Part> parts) {
		this.parts = parts;
	}

	/**
	 * @return the parameters
	 */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "question", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SELECT)
	public List<Parameter> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters
	 *            the parameters to set
	 */
	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public Object deepCopy() throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(this);
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bis);
		return ois.readObject();
	}
}
