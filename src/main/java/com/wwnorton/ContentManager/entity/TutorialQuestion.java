package com.wwnorton.ContentManager.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "PS_Tutorial_Question")
public class TutorialQuestion implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Tutorial Question表主键
	 */

	private int tutorialID;

	/**
	 * Parent Question ID
	 */

	private int questionID;

	/**
	 * Step Question ID
	 */

	private int stepQuestionID;

	/**
	 * Step Question的顺序
	 */

	private int stepIndex;

	/**
	 * Step Question的描述文本
	 */

	private String stepText;

	/**
	 * 创建时间
	 */

	private Date createTime;

	/**
	 * 修改时间
	 */

	private Date updateTime;

	/**
	 * Step Question活动标识
	 */

	private int stepActive = 0;

	public TutorialQuestion() {
	}

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Tutorial_ID", unique = true, nullable = false)
	public int getTutorialID() {
		return tutorialID;
	}

	public void setTutorialID(int tutorialID) {
		this.tutorialID = tutorialID;
	}

	@Column(name = "Question_ID")
	public int getQuestionID() {
		return questionID;
	}

	public void setQuestionID(int questionID) {
		this.questionID = questionID;
	}

	@Column(name = "Step_Question_ID")
	public int getStepQuestionID() {
		return stepQuestionID;
	}

	public void setStepQuestionID(int stepQuestionID) {
		this.stepQuestionID = stepQuestionID;
	}

	@Column(name = "Step_Index")
	public int getStepIndex() {
		return stepIndex;
	}

	public void setStepIndex(int stepIndex) {
		this.stepIndex = stepIndex;
	}

	@Column(name = "Step_Text")
	public String getStepText() {
		return stepText;
	}

	public void setStepText(String stepText) {
		this.stepText = stepText;
	}

	@Column(name = "Create_Time", updatable = false)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "Update_Time")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "Step_Active")
	public int getStepActive() {
		return stepActive;
	}

	public void setStepActive(int stepActive) {
		this.stepActive = stepActive;
	}
}
