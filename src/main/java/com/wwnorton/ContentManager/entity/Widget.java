package com.wwnorton.ContentManager.entity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_widget")
public class Widget implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2917470979187433911L;

	private Integer widgetId;

	private Integer questionID;

	private String widgetDivId;

	private String widgetName;

	private String creator;

	private String widgetData;

	private String imageData;

	private Integer widgetType;

	private Date createTime;

	private Date latestTime;

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "WidgetId", unique = true, nullable = false)
	public Integer getWidgetId() {
		return widgetId;
	}

	public void setWidgetId(Integer widgetId) {
		this.widgetId = widgetId;
	}

	@Column(name = "QuestionId")
	public int getQuestionID() {
		return questionID;
	}

	public void setQuestionID(int questionID) {
		this.questionID = questionID;
	}

	@Column(name = "WidgetDivId")
	public String getWidgetDivId() {
		return widgetDivId;
	}

	public void setWidgetDivId(String widgetDivId) {
		this.widgetDivId = widgetDivId;
	}

	@Column(name = "WidgetName")
	public String getWidgetName() {
		return widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

	@Column(name = "Creator")
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Column(name = "WidgetData")
	public String getWidgetData() {
		return widgetData;
	}

	public void setWidgetData(String widgetData) {
		this.widgetData = widgetData;
	}

	@Column(name = "ImageData")
	public String getImageData() {
		return imageData;
	}

	public void setImageData(String imageData) {
		this.imageData = imageData;
	}

	@Column(name = "WidgetType")
	public Integer getWidgetType() {
		return widgetType;
	}

	public void setWidgetType(Integer widgetType) {
		this.widgetType = widgetType;
	}

	@Column(name = "CreateTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "LatestTime", updatable = false, insertable = false)
	public Date getLatestTime() {
		return latestTime;
	}

	public void setLatestTime(Date latestTime) {
		this.latestTime = latestTime;
	}

	public Object deepCopy() throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(this);
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bis);
		return ois.readObject();
	}

}