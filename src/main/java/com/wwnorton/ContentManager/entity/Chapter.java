package com.wwnorton.ContentManager.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_chapter")
public class Chapter implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2292765709104468002L;

	private int chapterID;

	private Book book;

	private String chapterName;

	private List<Section> sections;

	private int orderIndex;

	private String questionNumberPrefix;

	private String resourceID;

	/**
	 * 
	 */
	public Chapter() {

	}

	/**
	 * 
	 * @param chapterID
	 * @param book
	 * @param chapterName
	 * @param sections
	 */
	public Chapter(int chapterID, Book book, String chapterName, List<Section> sections, int orderIndex) {
		super();
		this.chapterID = chapterID;
		this.book = book;
		this.chapterName = chapterName;
		this.sections = sections;
		this.orderIndex = orderIndex;
	}

	/**
	 * 
	 * @param chapterID
	 * @param chapterName
	 */
	public Chapter(int chapterID, String chapterName) {
		super();
		this.chapterID = chapterID;
		this.chapterName = chapterName;
	}

	/**
	 * 
	 * @param chapterID
	 * @param chapterName
	 */
	public Chapter(int chapterID, String chapterName, String questionNumberPrefix) {
		super();
		this.chapterID = chapterID;
		this.chapterName = chapterName;
		this.questionNumberPrefix = questionNumberPrefix;
	}

	/**
	 * 
	 * @param chapterID
	 * @param chapterName
	 */
	public Chapter(int chapterID, String chapterName, int orderIndex, String questionNumberPrefix) {
		super();
		this.chapterID = chapterID;
		this.chapterName = chapterName;
		this.questionNumberPrefix = questionNumberPrefix;
		this.orderIndex = orderIndex;
	}

	/**
	 * 
	 * @param chapterID
	 * @param chapterName
	 */
	public Chapter(int chapterID, String chapterName, int orderIndex) {
		super();
		this.chapterID = chapterID;
		this.chapterName = chapterName;
		this.orderIndex = orderIndex;
	}

	/**
	 * @return the chapterID
	 */
	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Chapter_ID", unique = true, nullable = false)
	public int getChapterID() {
		return chapterID;
	}

	/**
	 * @param chapterID
	 *            the chapterID to set
	 */
	public void setChapterID(int chapterID) {
		this.chapterID = chapterID;
	}

	/**
	 * @return the chapterName
	 */
	@Column(name = "Chapter_Name")
	public String getChapterName() {
		return chapterName;
	}

	/**
	 * @param chapterName
	 *            the chapterName to set
	 */
	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	/**
	 * @return the book
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Book_ID", nullable = false)
	public Book getBook() {
		return book;
	}

	/**
	 * @param book
	 *            the book to set
	 */
	public void setBook(Book book) {
		this.book = book;
	}

	/**
	 * @return the sections
	 */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "chapter", fetch = FetchType.EAGER)
	@OrderBy(value = "orderIndex ASC")
	@Fetch(value = FetchMode.SELECT)
	public List<Section> getSections() {
		return sections;
	}

	/**
	 * @param sections
	 *            the sections to set
	 */
	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

	@Column(name = "Order_Index")
	public int getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}

	@Column(name = "Question_Number_Prefix", insertable = false, updatable = false)
	public String getQuestionNumberPrefix() {
		return questionNumberPrefix;
	}

	public void setQuestionNumberPrefix(String questionNumberPrefix) {
		this.questionNumberPrefix = questionNumberPrefix;
	}

	@Column(name = "Resource_ID")
	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

}
