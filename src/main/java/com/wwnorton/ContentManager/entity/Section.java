package com.wwnorton.ContentManager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_section")
public class Section implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4764350195437438179L;

	private int sectionID;

	private Chapter chapter;

	private String key;

	private String sectionName;

	private String subManager;

	private int orderIndex;

	private String resourceID;

	public Section() {

	}

	public Section(int sectionID, Chapter chapter, String sectionName, String subManager) {
		super();
		this.sectionID = sectionID;
		this.chapter = chapter;
		this.key = String.valueOf(chapter.getChapterID()) + '-' + String.valueOf(sectionID);
		this.sectionName = sectionName;
		this.subManager = subManager;
	}

	public Section(int sectionID, String sectionName) {
		super();
		this.sectionID = sectionID;
		this.sectionName = sectionName;
	}

	public Section(int sectionID, String sectionName, int orderIndex) {
		super();
		this.sectionID = sectionID;
		this.sectionName = sectionName;
		this.orderIndex = orderIndex;
	}

	/**
	 * @return the sectionID
	 */

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Section_ID", unique = true, nullable = false)
	public int getSectionID() {
		return sectionID;
	}

	/**
	 * @param sectionID
	 *            the sectionID to set
	 */
	public void setSectionID(int sectionID) {
		this.sectionID = sectionID;
		if (null != this.chapter) {
			this.key = String.valueOf(this.chapter.getChapterID()) + '-' + String.valueOf(this.sectionID);
		}
	}

	/**
	 * @return the sectionName
	 */
	@Column(name = "Section_Name")
	public String getSectionName() {
		return sectionName;
	}

	/**
	 * @param sectionName
	 *            the sectionName to set
	 */
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	/**
	 * @return the chapter
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Chapter_ID", nullable = false)
	public Chapter getChapter() {
		return chapter;
	}

	/**
	 * @param chapter
	 *            the chapter to set
	 */
	public void setChapter(Chapter chapter) {
		this.chapter = chapter;
		this.key = String.valueOf(chapter.getChapterID()) + '-' + String.valueOf(this.sectionID);
	}

	@Column(name = "Order_Index")
	public int getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}

	@Column(name = "Resource_ID")
	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	@Transient
	public String getKey() {
		return this.key;
	}

}
