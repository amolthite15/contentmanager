package com.wwnorton.ContentManager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_question_learning_objective")
public class QuestionLearningObjective {

	private static final long serialVersionUID = 7427915221763961942L;

	private Integer questionObjectiveID;

	private Integer questionBookID;

	private Integer objectiveID;

	public QuestionLearningObjective() {

	}

	public QuestionLearningObjective(Integer questionBookID, Integer objectiveID) {
		super();
		this.questionBookID = questionBookID;
		this.objectiveID = objectiveID;
	}

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Question_Objective_ID", unique = true, nullable = false)
	public Integer getQuestionObjectiveID() {
		return questionObjectiveID;
	}

	/**
	 * 
	 * @param questionObjectiveID
	 */
	public void setQuestionObjectiveID(Integer questionObjectiveID) {
		this.questionObjectiveID = questionObjectiveID;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "Question_Book_ID")
	public Integer getQuestionBookID() {
		return questionBookID;
	}

	/**
	 * 
	 * @param question
	 */
	public void setQuestionBookID(Integer questionBookID) {
		this.questionBookID = questionBookID;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "Objective_ID")
	public Integer getObjectiveID() {
		return objectiveID;
	}

	/**
	 * 
	 * @param learningObjective
	 */
	public void setObjectiveID(Integer objectiveID) {
		this.objectiveID = objectiveID;
	}

	/**
	 * 
	 * @return
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
