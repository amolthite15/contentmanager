package com.wwnorton.ContentManager.entity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_question_book")
public class QuestionBook implements Serializable {

	private static final long serialVersionUID = -7427915221763961942L;

	private Integer questionBookID;

	private Integer questionID;

	private Integer bookID;

	private Integer chapterID;

	private Integer sectionID;

	private int mappingStatus;

	private Integer series;

	private String questionNumber;

	private Integer difficlutyRating;

	private Integer mappingOrigin = 0;

	private Integer mappingAction = 0;

	public QuestionBook() {

	}

	/**
	 * 
	 * @param questionBookID
	 * @param question
	 * @param book
	 * @param chapter
	 * @param section
	 */
	public QuestionBook(Integer questionBookID, Integer questionID, Integer bookID, Integer chapterID,
			Integer sectionID) {
		super();
		this.questionBookID = questionBookID;
		this.questionID = questionID;
		this.bookID = bookID;
		this.chapterID = chapterID;
		this.sectionID = sectionID;
	}

	/**
	 * 
	 * @return Integer.
	 */
	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Question_Book_ID", unique = true, nullable = false)
	public Integer getQuestionBookID() {
		return questionBookID;
	}

	/**
	 * 
	 * @param questionID
	 *            .
	 */
	public void setQuestionBookID(Integer questionBookID) {
		this.questionBookID = questionBookID;
	}

	/**
	 * @return the question
	 */
	@Column(name = "Question_ID")
	public Integer getQuestionID() {
		return questionID;
	}

	/**
	 * 
	 * @param question
	 */
	public void setQuestionID(Integer questionID) {
		this.questionID = questionID;
	}

	@Column(name = "Book_ID")
	public Integer getBookID() {
		return bookID;
	}

	public void setBookID(Integer bookID) {
		this.bookID = bookID;
	}

	@Column(name = "Chapter_ID")
	public Integer getChapterID() {
		return chapterID;
	}

	public void setChapterID(Integer chapterID) {
		this.chapterID = chapterID;
	}

	@Column(name = "Section_ID")
	public Integer getSectionID() {
		return sectionID;
	}

	public void setSectionID(Integer sectionID) {
		this.sectionID = sectionID;
	}

	@Column(name = "Mapping_Status")
	public int getMappingStatus() {
		return mappingStatus;
	}

	public void setMappingStatus(int mappingStatus) {
		this.mappingStatus = mappingStatus;
	}

	@Column(name = "Series")
	public Integer getSeries() {
		return series;
	}

	public void setSeries(Integer series) {
		this.series = series;
	}

	/**
	 * 
	 * @return String.
	 */
	@Column(name = "Question_Number")
	public String getQuestionNumber() {
		return questionNumber;
	}

	/**
	 * 
	 * @param questionNumber
	 *            .
	 */
	public void setQuestionNumber(String questionNumber) {
		this.questionNumber = questionNumber;
	}

	/**
	 * 
	 * @return Integer.
	 */
	@Column(name = "Difficluty_Rating")
	public Integer getDifficlutyRating() {
		return difficlutyRating;
	}

	/**
	 * 
	 * @return Integer.
	 */
	@Column(name = "Mapping_Origin")
	public Integer getMappingOrigin() {
		return mappingOrigin;
	}

	/**
	 * 
	 * @param mappingOrigin
	 *            .
	 */
	public void setMappingOrigin(Integer mappingOrigin) {
		this.mappingOrigin = mappingOrigin;
	}

	/**
	 * 
	 * @return Integer.
	 */
	@Column(name = "Mapping_Action")
	public Integer getMappingAction() {
		return mappingAction;
	}

	/**
	 * 
	 * @param mappingOrigin
	 *            .
	 */
	public void setMappingAction(Integer mappingAction) {
		this.mappingAction = mappingAction;
	}

	/**
	 * 
	 * @param difficlutyRating
	 *            .
	 */
	public void setDifficlutyRating(Integer difficlutyRating) {
		this.difficlutyRating = difficlutyRating;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Object deepCopy() throws IOException, ClassNotFoundException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(this);
		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bis);
		return ois.readObject();
	}

}