package com.wwnorton.ContentManager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ps_book_series")
public class BookSeries {

	private Integer bookSeriesID;

	private Integer bookID;

	private String series;

	private String descriptiveName;

	private int isApplySeriesFilter;

	@Id
	@GeneratedValue(generator = "generator")
	@GenericGenerator(name = "generator", strategy = "native")
	@Column(name = "Book_Series_ID", unique = true, nullable = false)
	public Integer getBookSeriesID() {
		return bookSeriesID;
	}

	public void setBookSeriesID(Integer bookSeriesID) {
		this.bookSeriesID = bookSeriesID;
	}

	@Column(name = "Book_ID")
	public Integer getBookID() {
		return bookID;
	}

	public void setBookID(Integer bookID) {
		this.bookID = bookID;
	}

	@Column(name = "Series")
	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	@Column(name = "Descriptive_Name")
	public String getDescriptiveName() {
		return descriptiveName;
	}

	public void setDescriptiveName(String descriptiveName) {
		this.descriptiveName = descriptiveName;
	}

	@Column(name = "Is_Apply_Series_Filter")
	public int getIsApplySeriesFilter() {
		return isApplySeriesFilter;
	}

	public void setIsApplySeriesFilter(int isApplySeriesFilter) {
		this.isApplySeriesFilter = isApplySeriesFilter;
	}

}
