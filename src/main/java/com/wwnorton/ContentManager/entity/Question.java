package com.wwnorton.ContentManager.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "ps_question")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "discriminator", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("Q")
public class Question implements Serializable {
  
  private static final long serialVersionUID = -7427915221763961942L;

  private Integer questionID;
  
  private String questionTitle;

  private String questionDescription;
  
  private String questionType;
  
  private String questionSnippet;

  private String demoteNote;

  private String liveEditNote;
  
  private String bloomsTypeDomain;
  
  private Integer questionState;
  
  private String solutionText;

  private String author;
  
  private Date createTime;
  
  private String introductionText;
  
  private Double grade = 0.0;
  
  private String creator;
  
  private String reviewer;
  
  private Date latestTime;
  
  private Integer wAuthor;
  
  private Integer wReviewer;
  
  private Integer wCopyeditor;
  
  private Integer wQATester;
  
  private Integer wEditor;
  
  private Integer published;

  private Integer demoted;
  
  private Integer lastOperator = 0;

  private Integer questionScope;

  private String comment;

  private Integer itemID = 0;
  
  private Integer questionVersion = 1;

  private Integer demoteType = 0;
  
  private Integer questionFrom = 0;
  
  private Integer displayAuthor;

  private String questionMisc;
  
  private Integer decimalPlaces;

  private Integer significantFigures;
  
  private Integer isDemoQuestion = 0;

  private Integer isTutorial = 0;

  private Integer poolState = 0;
  
  private Integer hasAlgo = 0;
  
  private Integer commaSeparate = 0; 
  
  private Integer guessPenalty = 0; 
  
  public Question() {}
  
  public Question(Integer questionID, String questionTitle, String questionType,
      String questionSnippet, String bloomsTypeDomain, Integer questionState, Double grade,
      Integer questionScope, Integer isDemoQuestion, Integer isTutorial, Integer poolState,
      Integer hasAlgo, Integer commaSeparate, Integer guessPenalty) {
    this.questionID = questionID;
    this.questionTitle = questionTitle;
    this.questionType = questionType;
    this.questionSnippet = questionSnippet;
    this.bloomsTypeDomain = bloomsTypeDomain;
    this.questionState = questionState;
    this.grade = grade;
    this.questionScope = questionScope;
    this.isDemoQuestion = isDemoQuestion;
    this.isTutorial = isTutorial;
    this.poolState = poolState;
    this.hasAlgo = hasAlgo;
    this.commaSeparate = commaSeparate;
    this.guessPenalty = guessPenalty;
  }

  /**
   * for adding or editing activity
   * 
   * @param questionID
   * @param questionNumber
   * @param questionTitle
   * @param difficlutyRating
   * @param questionState
   * @param grade
   */
  public Question(Integer questionID, String questionTitle, Integer questionState, Double grade) {
    super();
    this.questionID = questionID;
    this.questionTitle = questionTitle;
    this.questionState = questionState;
    this.grade = grade;
  }

  /**
   * 
   * @return .
   */
  @Column(name = "Question_State")
  public Integer getQuestionState() {
    return questionState;
  }

  /**
   * @see whether this record could query and use,If value equal to 0 can use,or could not .
   * @param questionState .
   */
  public void setQuestionState(Integer questionState) {
    this.questionState = questionState;
  }

  /**
   * 
   * @return Integer.
   */
  @Id
  @GeneratedValue(generator = "generator")
  @GenericGenerator(name = "generator", strategy = "native")
  @Column(name = "Question_ID", unique = true, nullable = false)
  public Integer getQuestionID() {
    return questionID;
  }

  /**
   * 
   * @param questionID .
   */
  public void setQuestionID(Integer questionID) {
    this.questionID = questionID;
  }

  /**
   * 
   * @return String.
   */
  @Column(name = "Question_Title")
  public String getQuestionTitle() {
    return questionTitle;
  }

  /**
   * 
   * @param questionTitle .
   */
  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  /**
   * 
   * @return String.
   */
  @Column(name = "Description")
  public String getQuestionDescription() {
    return questionDescription;
  }

  /**
   * s
   * 
   * @param questionDescription .
   */
  public void setQuestionDescription(String questionDescription) {
    this.questionDescription = questionDescription;
  }

  /**
   * 
   * @return String.
   */
  @Column(name = "Blooms_Type_domain")
  public String getBloomsTypeDomain() {
    return bloomsTypeDomain;
  }

  /**
   * 
   * @param bloomsTypeDomain .
   */
  public void setBloomsTypeDomain(String bloomsTypeDomain) {
    this.bloomsTypeDomain = bloomsTypeDomain;
  }

  /**
   * @return the solutionText
   */
  @Column(name = "Solution_Text")
  public String getSolutionText() {
    return solutionText;
  }

  /**
   * @param solutionText the solutionText to set
   */
  public void setSolutionText(String solutionText) {
    this.solutionText = solutionText;
  }

  /**
   * 
   * @return author
   */
  @Column(name = "Question_Author")
  public String getAuthor() {
    return author;
  }

  /**
   * 
   * @param author .
   */
  public void setAuthor(String author) {
    this.author = author;
  }

  /**
   * 
   * @return createTime
   */
  @Column(name = "Create_Time", updatable = false)
  public Date getCreateTime() {
    return createTime;
  }

  /**
   * 
   * @param createTime .
   */
  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  /**
   * 
   * @return String.
   */
  @Column(name = "Introduction_Text")
  public String getIntroductionText() {
    return introductionText;
  }

  /**
   * 
   * @param introductionText
   */
  public void setIntroductionText(String introductionText) {
    this.introductionText = introductionText;
  }

  /**
   * 
   * @return Double
   */
  @Column(name = "Grade")
  public Double getGrade() {
    return grade;
  }

  /**
   * 
   * @param grade
   */
  public void setGrade(Double grade) {
    this.grade = grade;
  }

  /**
   * 
   * @return author
   */
  @Column(name = "Creator")
  public String getCreator() {
    return creator;
  }

  /**
   * 
   * @param author .
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * 
   * @return author
   */
  @Column(name = "Reviewer")
  public String getReviewer() {
    return reviewer;
  }

  /**
   * 
   * @param author .
   */
  public void setReviewer(String reviewer) {
    this.reviewer = reviewer;
  }

  /**
   * 
   * @return author
   */
  @Column(name = "Latest_Time", updatable = false)
  public Date getLatestTime() {
    return latestTime;
  }

  /**
   * 
   * @param author .
   */
  public void setLatestTime(Date latestTime) {
    this.latestTime = latestTime;
  }

  /**
   * @return wAuthor
   */
  @Column(name = "W_Author", insertable = false, updatable = false)
  public Integer getwAuthor() {
    return wAuthor;
  }

  /**
   * 
   * @param wAuthor
   */
  public void setwAuthor(Integer wAuthor) {
    this.wAuthor = wAuthor;
  }

  /**
   * @return wReviewer
   */
  @Column(name = "W_Reviewer", insertable = false, updatable = false)
  public Integer getwReviewer() {
    return wReviewer;
  }

  /**
   * 
   * @param wReviewer
   */
  public void setwReviewer(Integer wReviewer) {
    this.wReviewer = wReviewer;
  }

  /**
   * @return wCopyeditor
   */
  @Column(name = "W_Copyeditor", insertable = false, updatable = false)
  public Integer getwCopyeditor() {
    return wCopyeditor;
  }

  /**
   * 
   * @param wCopyeditor
   */
  public void setwCopyeditor(Integer wCopyeditor) {
    this.wCopyeditor = wCopyeditor;
  }

  /**
   * @return wQATester
   */
  @Column(name = "W_QATester", insertable = false, updatable = false)
  public Integer getwQATester() {
    return wQATester;
  }

  /**
   * 
   * @param wQATester
   */
  public void setwQATester(Integer wQATester) {
    this.wQATester = wQATester;
  }

  /**
   * @return wEditor
   */
  @Column(name = "W_Editor", insertable = false, updatable = false)
  public Integer getwEditor() {
    return wEditor;
  }

  /**
   * 
   * @param wEditor
   */
  public void setwEditor(Integer wEditor) {
    this.wEditor = wEditor;
  }

  /**
   * @return published
   */
  @Column(name = "Published", insertable = false, updatable = false)
  public Integer getPublished() {
    return published;
  }

  /**
   * 
   * @param published
   */
  public void setPublished(Integer published) {
    this.published = published;
  }

  /**
   * @return demoted
   */
  @Column(name = "Demoted", insertable = false, updatable = false)
  public Integer getDemoted() {
    return demoted;
  }

  /**
   * 
   * @param demoted
   */
  public void setDemoted(Integer demoted) {
    this.demoted = demoted;
  }

  /**
   * @return lastOperator
   */
  @Column(name = "Last_Operator", insertable = false)
  public Integer getLastOperator() {
    return lastOperator;
  }

  /**
   * 
   * @param lastOperator
   */
  public void setLastOperator(Integer lastOperator) {
    this.lastOperator = lastOperator;
  }

  /**
   * 
   * @return questionScope
   */
  @Column(name = "Question_Scope")
  public Integer getQuestionScope() {
    return questionScope;
  }

  /**
   * 
   * @param questionScope
   */
  public void setQuestionScope(Integer questionScope) {
    this.questionScope = questionScope;
  }

  /**
   * 
   * @return displayAuthor
   */
  @Column(name = "Display_Author")
  public Integer getDisplayAuthor() {
    return displayAuthor;
  }

  /**
   * 
   * @param displayAuthor
   */
  public void setDisplayAuthor(Integer displayAuthor) {
    this.displayAuthor = displayAuthor;
  }

  @Column(name = "Comment_Text")
  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Column(name = "Item_ID")
  public Integer getItemID() {
    return itemID;
  }

  public void setItemID(Integer itemID) {
    this.itemID = itemID;
  }

  @Column(name = "Question_Version")
  public Integer getQuestionVersion() {
    return questionVersion;
  }

  public void setQuestionVersion(Integer questionVersion) {
    this.questionVersion = questionVersion;
  }

  @Column(name = "Demote_Type", insertable = false)
  public Integer getDemoteType() {
    return demoteType;
  }

  public void setDemoteType(Integer demoteType) {
    this.demoteType = demoteType;
  }

  @Column(name = "Question_From")
  public Integer getQuestionFrom() {
    return questionFrom;
  }

  public void setQuestionFrom(Integer questionFrom) {
    this.questionFrom = questionFrom;
  }

  @Column(name = "Question_Misc")
  public String getQuestionMisc() {
    return questionMisc;
  }

  public void setQuestionMisc(String questionMisc) {
    this.questionMisc = questionMisc;
  }

  @Column(name = "Significant_Figures", nullable = false, columnDefinition = "int default 1")
  public Integer getSignificantFigures() {
    return significantFigures;
  }

  public void setSignificantFigures(Integer significantFigures) {
    this.significantFigures = significantFigures;
  }

  @Column(name = "Decimal_Places", nullable = false, columnDefinition = "int default 2")
  public Integer getDecimalPlaces() {
    return decimalPlaces;
  }

  public void setDecimalPlaces(Integer decimalPlaces) {
    this.decimalPlaces = decimalPlaces;
  }

  @Column(name = "Question_Type")
  public String getQuestionType() {
    return questionType;
  }

  public void setQuestionType(String questionType) {
    this.questionType = questionType;
  }

  @Column(name = "Question_Snippet")
  public String getQuestionSnippet() {
    return questionSnippet;
  }

  public void setQuestionSnippet(String questionSnippet) {
    this.questionSnippet = questionSnippet;
  }

  @Column(name = "Demote_Note")
  public String getDemoteNote() {
    return demoteNote;
  }

  public void setDemoteNote(String demoteNote) {
    this.demoteNote = demoteNote;
  }

  @Column(name = "LiveEdit_Note")
  public String getLiveEditNote() {
    return liveEditNote;
  }

  public void setLiveEditNote(String liveEditNote) {
    this.liveEditNote = liveEditNote;
  }

  @Column(name = "Is_Demo_Question")
  public Integer getIsDemoQuestion() {
    return isDemoQuestion;
  }

  public void setIsDemoQuestion(Integer isDemoQuestion) {
    this.isDemoQuestion = isDemoQuestion;
  }

  @Column(name = "Is_Tutorial")
  public Integer getIsTutorial() {
    return isTutorial;
  }

  public void setIsTutorial(Integer isTutorial) {
    this.isTutorial = isTutorial;
  }

  @Column(name = "Pool_State")
  public Integer getPoolState() {
    return poolState;
  }

  public void setPoolState(Integer poolState) {
    this.poolState = poolState;
  }

  @Column(name = "Has_Algo")
  public Integer getHasAlgo() {
    return hasAlgo;
  }

  public void setHasAlgo(Integer hasAlgo) {
    this.hasAlgo = hasAlgo;
  }

  @Column(name = "Comma_Separate")
  public Integer getCommaSeparate() {
    return commaSeparate;
  }

  public void setCommaSeparate(Integer commaSeparate) {
    this.commaSeparate = commaSeparate;
  }
  
  @Column(name = "Guess_Penalty")
  public Integer getGuessPenalty() {
    return guessPenalty;
  }

  public void setGuessPenalty(Integer guessPenalty) {
    this.guessPenalty = guessPenalty;
  }
}