package com.wwnorton.ContentManager.util;

import java.io.Writer;
import java.util.List;

public class CSVUtils {

	private static final char DEFAULT_SEPARATOR = ',';

	public static void writeLine(Writer writer, List<String> values) throws Exception {
		writeLine(writer, values, DEFAULT_SEPARATOR);
	}

	private static String followCSVFormat(String value) {
		String result = value;
		if (result.contains("\"")) {
			result = result.replace("\"", "\"\"");
		}
		return result;
	}

	public static void writeLine(Writer writer, List<String> values, char separators) throws Exception {
		boolean first = false;

		if (separators == ' ') {
			separators = DEFAULT_SEPARATOR;
		}

		StringBuilder builder = new StringBuilder();
		for (String value : values) {
			if (!first) {
				builder.append(",");
			}
			builder.append(followCSVFormat(value));
			
			first = false;
		}

		builder.append("\n");

		writer.append(builder.toString());
	}

}
