package com.wwnorton.ContentManager.util;

import java.util.HashMap;
import java.util.Map;

public class ChapterUtil {
	private static Map<Integer, Integer> chapterMap = new HashMap<Integer, Integer>();

	static {
		chapterMap.put(0, 0);
		chapterMap.put(1, 1);
		chapterMap.put(2, 3);
		chapterMap.put(3, 4);
		chapterMap.put(4, 4);
		chapterMap.put(5, 5);
		chapterMap.put(6, 5);
		chapterMap.put(7, 10);
		chapterMap.put(8, 6);
		chapterMap.put(9, 7);
		chapterMap.put(10, 8);
		chapterMap.put(11, 0);
		chapterMap.put(12, 8);
		chapterMap.put(13, 11);
		chapterMap.put(14, 9);
		chapterMap.put(15, 9);
		chapterMap.put(16, 0);
		chapterMap.put(17, 11);
		chapterMap.put(18, 0);
		chapterMap.put(19, 12);
		chapterMap.put(20, 13);
		chapterMap.put(21, 13);
		chapterMap.put(22, 16);
		chapterMap.put(23, 16);
		chapterMap.put(24, 15);
		chapterMap.put(25, 0);
		chapterMap.put(26, 14);
		chapterMap.put(27, 12);
		chapterMap.put(28, 19);
		chapterMap.put(29, 19);
		chapterMap.put(30, 17);
		chapterMap.put(31, 18);
		chapterMap.put(32, 20);
		chapterMap.put(33, 20);
	}

	public static Integer getMappedChapterNumber(Integer chapterNumber) {
		if (null != chapterNumber) {

			return chapterMap.get(chapterNumber);
		}

		return 0;
	}

}
