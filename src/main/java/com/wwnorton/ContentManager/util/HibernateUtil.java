package com.wwnorton.ContentManager.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.wwnorton.ContentManager.entity.Book;
import com.wwnorton.ContentManager.entity.BookAuthor;
import com.wwnorton.ContentManager.entity.BookSeries;
import com.wwnorton.ContentManager.entity.Chapter;
import com.wwnorton.ContentManager.entity.Components;
import com.wwnorton.ContentManager.entity.Graph;
import com.wwnorton.ContentManager.entity.Parameter;
import com.wwnorton.ContentManager.entity.Part;
import com.wwnorton.ContentManager.entity.PoolQuestion;
import com.wwnorton.ContentManager.entity.Question;
import com.wwnorton.ContentManager.entity.QuestionBook;
import com.wwnorton.ContentManager.entity.QuestionForRest;
import com.wwnorton.ContentManager.entity.QuestionLearningObjective;
import com.wwnorton.ContentManager.entity.QuestionLearningOutcomes;
import com.wwnorton.ContentManager.entity.Section;
import com.wwnorton.ContentManager.entity.TutorialQuestion;
import com.wwnorton.ContentManager.entity.Widget;

public class HibernateUtil {

	private static SessionFactory sessionFactory;
	private static Session session = null;

	public static void loadDatabaseConfiguration(File file) throws Exception {
		System.out.println("-------- loading database configurations -----------");
//		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
//		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
//
//		sessionFactory = meta.getSessionFactoryBuilder().build();

		 Properties properties = new Properties();
		 properties.load(new FileInputStream(file));
		
		 Configuration configuration = new Configuration();
		 configuration.addProperties(properties);
		 StandardServiceRegistry serviceRegistry = new
		 StandardServiceRegistryBuilder()
		 .applySettings(configuration.getProperties()).build();
		 configuration.addAnnotatedClass(BookAuthor.class);
		 configuration.addAnnotatedClass(Book.class);
		 configuration.addAnnotatedClass(Chapter.class);
		 configuration.addAnnotatedClass(Section.class);
		 configuration.addAnnotatedClass(QuestionBook.class);
		 configuration.addAnnotatedClass(Question.class);
		 configuration.addAnnotatedClass(QuestionForRest.class);
		 configuration.addAnnotatedClass(Part.class);
		 configuration.addAnnotatedClass(Components.class);
		 configuration.addAnnotatedClass(Widget.class);
		 configuration.addAnnotatedClass(Parameter.class);
		 configuration.addAnnotatedClass(Graph.class);
		 configuration.addAnnotatedClass(PoolQuestion.class);
		 configuration.addAnnotatedClass(TutorialQuestion.class);
		 configuration.addAnnotatedClass(QuestionLearningObjective.class);
		 configuration.addAnnotatedClass(QuestionLearningOutcomes.class);
		 configuration.addAnnotatedClass(BookSeries.class);
		
		 sessionFactory = configuration.buildSessionFactory(serviceRegistry);

		System.out.println("------ Database configuration loaded successfully ---------");
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static Session getCurrentSession() {
		if (null == session) {
			System.out.println("--- Creating a brand new session -----");
			session = sessionFactory.openSession();
		}

		return session;
	}

	public static void closeSession() {
		if (null != session && session.isOpen()) {
			session.close();
		}
		session = null;
	}

	public static void closeSessionFactory() {
		if (null != session && session.isOpen()) {
			session.close();
		}
		sessionFactory.close();
	}

}
