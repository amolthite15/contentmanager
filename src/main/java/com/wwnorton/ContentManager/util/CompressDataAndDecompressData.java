package com.wwnorton.ContentManager.util;

import java.io.ByteArrayOutputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;
import org.imsglobal.basiclti.Base64;

public class CompressDataAndDecompressData {
	public String compressData(String data) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			DeflaterOutputStream zos = new DeflaterOutputStream(bos);
			zos.write(data.getBytes());
			zos.close();
			return new String(getenBASE64inCodec(bos.toByteArray()));
		} catch (Exception ex) {
			System.out.println(ex.getMessage() + ", " + ex);
			return "ZIP_ERR";
		}
	}

	public String getenBASE64inCodec(byte[] b) {
		if (b == null)
			return null;
		return new String(Base64.encode(b));
	}

	public static byte[] getdeBASE64inCodec(String s) {
		if (s == null)
			return null;
		return Base64.decode(s.getBytes());
	}

	public String decompressData(String encdata) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			InflaterOutputStream zos = new InflaterOutputStream(bos);
			zos.write(getdeBASE64inCodec(encdata));
			zos.close();
			return new String(bos.toByteArray());
		} catch (Exception ex) {
			System.out.println("there is unnecessary to decompress the Data.");
			return encdata;
		}
	}
}
