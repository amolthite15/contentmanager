package com.wwnorton.ContentManager;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.wwnorton.ContentManager.model.QuestionReport;
import com.wwnorton.ContentManager.util.CSVUtils;

public class QuestionReportManager {

	String filePath = "/Users/athite/Desktop";

	public void writeToFile(String fileName, List<QuestionReport> reports) throws Exception {

		String projectDirectory = System.getProperty("user.dir");
		System.out.println("Project Directory : " + projectDirectory);
		String file = projectDirectory + "/" + fileName;
		FileWriter fileWriter = new FileWriter(file);

		CSVUtils.writeLine(fileWriter, Arrays.asList("Old Question Id, New Question Id, Status, Comments"));

		for (QuestionReport report : reports) {
			List<String> list = new ArrayList<String>();
			list.add(Integer.toString(report.getOldQuestionId()));
			list.add(Integer.toString(report.getNewQuestionId()));
			list.add(report.getStatus());
			list.add(report.getComments());

			CSVUtils.writeLine(fileWriter, list);
		}

		fileWriter.flush();
		fileWriter.close();

	}
}
