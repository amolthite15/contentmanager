package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.entity.BookSeries;

public interface BookSeriesService {
	int getBookSerise(int bookID, String serise);

	void saveBookSeries(BookSeries bookSeries);
}
