package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.entity.BookAuthor;

public interface BookAuthorService {
	
	BookAuthor findBookAuthor(String userID, int bookID);

}
