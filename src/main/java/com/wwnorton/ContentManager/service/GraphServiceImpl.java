package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.dao.GraphDao;
import com.wwnorton.ContentManager.dao.GraphDaoImpl;
import com.wwnorton.ContentManager.entity.Graph;

public class GraphServiceImpl implements GraphService {
	private GraphDao graphDao = new GraphDaoImpl();

	public Graph findGraphByImgID(String id) {
		return this.graphDao.findGraphByImgID(id);
	}

	public void saveGraphForObject(Graph graph) {
		this.graphDao.saveGraphForObject(graph);
	}

}
