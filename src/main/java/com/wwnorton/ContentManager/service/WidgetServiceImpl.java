package com.wwnorton.ContentManager.service;

import java.util.List;

import com.wwnorton.ContentManager.dao.WidgetDao;
import com.wwnorton.ContentManager.dao.WidgetDaoImpl;
import com.wwnorton.ContentManager.entity.Widget;

public class WidgetServiceImpl implements WidgetService {

	private WidgetDao widgetDao = new WidgetDaoImpl();

	public void saveOrUpdateWidget(Widget widget) {
		this.widgetDao.saveOrUpdateWidget(widget);
	}

	public List<Widget> getWidgetByQuestionId(int questionId) {
		return this.widgetDao.getWidgetByQuestionId(questionId);
	}

}
