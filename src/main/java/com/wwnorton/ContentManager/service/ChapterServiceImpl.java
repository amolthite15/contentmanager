package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.dao.ChapterDao;
import com.wwnorton.ContentManager.dao.ChapterDaoImpl;
import com.wwnorton.ContentManager.entity.Chapter;

public class ChapterServiceImpl implements ChapterService {

	private ChapterDao chapterDao = new ChapterDaoImpl();

	public Chapter findChapter(int chapterID) {

		return chapterDao.findChapter(chapterID);
	}

	public Chapter findChapter(int bookId, int orderIndex) {

		return chapterDao.findChapter(bookId, orderIndex);
	}

}
