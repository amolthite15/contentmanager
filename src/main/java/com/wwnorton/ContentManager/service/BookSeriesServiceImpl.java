package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.dao.BookSeriesDao;
import com.wwnorton.ContentManager.dao.BookSeriesDaoImpl;
import com.wwnorton.ContentManager.entity.BookSeries;

public class BookSeriesServiceImpl implements BookSeriesService {

	private BookSeriesDao bookSeriesDao = new BookSeriesDaoImpl();

	public int getBookSerise(int bookID, String serise) {
		return this.bookSeriesDao.getBookSerise(bookID, serise);
	}

	public void saveBookSeries(BookSeries bookSeries) {
		this.bookSeriesDao.saveBookSeries(bookSeries);
	}

}
