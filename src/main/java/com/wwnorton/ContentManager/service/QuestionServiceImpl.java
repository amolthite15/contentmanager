package com.wwnorton.ContentManager.service;

import java.util.List;

import com.wwnorton.ContentManager.dao.QuestionDao;
import com.wwnorton.ContentManager.dao.QuestionDaoImpl;
import com.wwnorton.ContentManager.entity.Question;
import com.wwnorton.ContentManager.entity.QuestionBook;
import com.wwnorton.ContentManager.entity.QuestionForRest;

public class QuestionServiceImpl implements QuestionService {

	private QuestionDao questionDao = new QuestionDaoImpl();

	public List<Integer> getQuestionIdsByBookId(Integer bookId) {

		return questionDao.getQuestionIdsByBookId(bookId);
	}

	public QuestionForRest findParamsRadioByID(Integer id) {

		return questionDao.findParamsRadioByID(id);
	}

	public Question saveQuestionForRest(Question question) {

		return questionDao.saveQuestionForRest(question);
	}

	public void update(Question question) {
		questionDao.update(question);
	}

	public QuestionBook getQuestionBook(int questionId, int bookId) {

		return questionDao.getQuestionBook(questionId, bookId);
	}

	public void saveQuestionBook(QuestionBook questionBook) {
		questionDao.saveQuestionBook(questionBook);
	}

	public void cloneObjectives(Integer sourceQuestionBookID, Integer targetQuestionBookID) {
		questionDao.cloneObjectives(sourceQuestionBookID, targetQuestionBookID);
	}

	public void cloneOutComes(Integer sourceQuestionBookID, Integer targetQuestionBookID, Integer targetQuestionId) {
		questionDao.cloneOutComes(sourceQuestionBookID, targetQuestionBookID, targetQuestionId);
	}

}
