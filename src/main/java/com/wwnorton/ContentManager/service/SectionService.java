package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.entity.Section;

public interface SectionService {

	Section getSectionsByChapterID(int chapterID);

}
