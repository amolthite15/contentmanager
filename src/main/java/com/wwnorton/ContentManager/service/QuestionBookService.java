package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.entity.QuestionBook;

public interface QuestionBookService {

	QuestionBook getQuestionBook(int questionId, int bookId);

}
