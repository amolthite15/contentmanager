package com.wwnorton.ContentManager.service;

import java.util.List;

import com.wwnorton.ContentManager.entity.Widget;

public interface WidgetService {

	void saveOrUpdateWidget(Widget widget);
	
	List<Widget> getWidgetByQuestionId(int questionId);

}
