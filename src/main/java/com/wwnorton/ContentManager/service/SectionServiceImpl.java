package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.dao.SectionDao;
import com.wwnorton.ContentManager.dao.SectionDaoImpl;
import com.wwnorton.ContentManager.entity.Section;

public class SectionServiceImpl implements SectionService {

	private SectionDao sectionDao = new SectionDaoImpl();

	public Section getSectionsByChapterID(int chapterID) {

		return sectionDao.getSectionsByChapterID(chapterID);
	}

}
