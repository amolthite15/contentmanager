package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.dao.QuestionBookDao;
import com.wwnorton.ContentManager.dao.QuestionBookDaoImpl;
import com.wwnorton.ContentManager.entity.QuestionBook;

public class QuestionBookServiceImpl implements QuestionBookService {

	private QuestionBookDao questionBookDao = new QuestionBookDaoImpl();

	public QuestionBook getQuestionBook(int questionId, int bookId) {
		return this.questionBookDao.getQuestionBook(questionId, bookId);
	}

}
