package com.wwnorton.ContentManager.service;

import java.util.List;

import com.wwnorton.ContentManager.dao.TutorialQuestionDao;
import com.wwnorton.ContentManager.dao.TutorialQuestionDaoImpl;
import com.wwnorton.ContentManager.entity.TutorialQuestion;

public class TutorialQuestionServiceImpl implements TutorialQuestionService {

	private TutorialQuestionDao tutorialQuestionDao = new TutorialQuestionDaoImpl();

	public TutorialQuestion saveTutorialQuestion(TutorialQuestion tutorialQuestion) {
		return this.tutorialQuestionDao.saveTutorialQuestion(tutorialQuestion);
	}

	public List<Integer> getStepQuestionIDsByParentID(Integer questionID) {
		return this.tutorialQuestionDao.getStepQuestionIDsByParentID(questionID);
	}

	public TutorialQuestion getTutQuesionByStepQID(int stepQID) {
		return this.tutorialQuestionDao.getTutQuesionByStepQID(stepQID);
	}

}
