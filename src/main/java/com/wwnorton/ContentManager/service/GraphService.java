package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.entity.Graph;

public interface GraphService {

	Graph findGraphByImgID(String id);

	void saveGraphForObject(Graph graph);
}
