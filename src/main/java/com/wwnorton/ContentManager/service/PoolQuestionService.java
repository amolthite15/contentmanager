package com.wwnorton.ContentManager.service;

import java.util.List;

import com.wwnorton.ContentManager.entity.PoolQuestion;

public interface PoolQuestionService {

	List<PoolQuestion> getValidPoolQuesByPoolIdWithOrder(int poolId);

	PoolQuestion savePoolQue(PoolQuestion poolQue);

}
