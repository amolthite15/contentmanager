package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.entity.Chapter;

public interface ChapterService {
	
	Chapter findChapter(int chapterID);
	
	Chapter findChapter(int bookId, int orderIndex);

}
