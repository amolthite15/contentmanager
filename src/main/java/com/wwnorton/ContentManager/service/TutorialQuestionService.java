package com.wwnorton.ContentManager.service;

import java.util.List;

import com.wwnorton.ContentManager.entity.TutorialQuestion;

public interface TutorialQuestionService {

	TutorialQuestion saveTutorialQuestion(TutorialQuestion tutorialQuestion);

	List<Integer> getStepQuestionIDsByParentID(Integer questionID);

	TutorialQuestion getTutQuesionByStepQID(int stepQID);

}
