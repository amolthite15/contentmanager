package com.wwnorton.ContentManager.service;

import com.wwnorton.ContentManager.dao.BookAuthorDao;
import com.wwnorton.ContentManager.dao.BookAuthorDaoImpl;
import com.wwnorton.ContentManager.entity.BookAuthor;

public class BookAuthorServiceImpl implements BookAuthorService {

	private BookAuthorDao bookAuthorDao = new BookAuthorDaoImpl();

	public BookAuthor findBookAuthor(String userID, int bookID) {
		
		return this.bookAuthorDao.findBookAuthor(userID,bookID);
	}

}
