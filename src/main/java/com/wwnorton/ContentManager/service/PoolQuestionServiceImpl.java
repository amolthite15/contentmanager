package com.wwnorton.ContentManager.service;

import java.util.List;

import com.wwnorton.ContentManager.dao.PoolQuestionDao;
import com.wwnorton.ContentManager.dao.PoolQuestionDaoImpl;
import com.wwnorton.ContentManager.entity.PoolQuestion;

public class PoolQuestionServiceImpl implements PoolQuestionService {

	private PoolQuestionDao poolQuestionDao = new PoolQuestionDaoImpl();

	public List<PoolQuestion> getValidPoolQuesByPoolIdWithOrder(int poolId) {
		return this.poolQuestionDao.getValidPoolQuesByPoolIdWithOrder(poolId);
	}

	public PoolQuestion savePoolQue(PoolQuestion poolQue) {
		return this.poolQuestionDao.savePoolQue(poolQue);
	}

}
