package com.wwnorton.ContentManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.wwnorton.ContentManager.entity.BookAuthor;
import com.wwnorton.ContentManager.entity.BookSeries;
import com.wwnorton.ContentManager.entity.Chapter;
import com.wwnorton.ContentManager.entity.Components;
import com.wwnorton.ContentManager.entity.Graph;
import com.wwnorton.ContentManager.entity.Parameter;
import com.wwnorton.ContentManager.entity.Part;
import com.wwnorton.ContentManager.entity.PoolQuestion;
import com.wwnorton.ContentManager.entity.Question;
import com.wwnorton.ContentManager.entity.QuestionBook;
import com.wwnorton.ContentManager.entity.QuestionForRest;
import com.wwnorton.ContentManager.entity.Section;
import com.wwnorton.ContentManager.entity.TutorialQuestion;
import com.wwnorton.ContentManager.entity.Widget;
import com.wwnorton.ContentManager.model.QuestionReport;
import com.wwnorton.ContentManager.service.BookAuthorService;
import com.wwnorton.ContentManager.service.BookAuthorServiceImpl;
import com.wwnorton.ContentManager.service.BookSeriesService;
import com.wwnorton.ContentManager.service.BookSeriesServiceImpl;
import com.wwnorton.ContentManager.service.ChapterService;
import com.wwnorton.ContentManager.service.ChapterServiceImpl;
import com.wwnorton.ContentManager.service.GraphService;
import com.wwnorton.ContentManager.service.GraphServiceImpl;
import com.wwnorton.ContentManager.service.PoolQuestionService;
import com.wwnorton.ContentManager.service.PoolQuestionServiceImpl;
import com.wwnorton.ContentManager.service.QuestionService;
import com.wwnorton.ContentManager.service.QuestionServiceImpl;
import com.wwnorton.ContentManager.service.SectionService;
import com.wwnorton.ContentManager.service.SectionServiceImpl;
import com.wwnorton.ContentManager.service.TutorialQuestionService;
import com.wwnorton.ContentManager.service.TutorialQuestionServiceImpl;
import com.wwnorton.ContentManager.service.WidgetService;
import com.wwnorton.ContentManager.service.WidgetServiceImpl;
import com.wwnorton.ContentManager.util.ChapterUtil;
import com.wwnorton.ContentManager.util.CompressDataAndDecompressData;
import com.wwnorton.ContentManager.util.Constants;
import com.wwnorton.ContentManager.util.HibernateUtil;

import net.sf.json.JSONObject;

public class ContentHandler {

	private QuestionService questionService = new QuestionServiceImpl();
	private BookAuthorService bookAuthorService = new BookAuthorServiceImpl();
	private GraphService graphService = new GraphServiceImpl();
	private TutorialQuestionService tutorialQuestionService = new TutorialQuestionServiceImpl();
	private WidgetService widgetService = new WidgetServiceImpl();
	private BookSeriesService bookSeriesService = new BookSeriesServiceImpl();
	private PoolQuestionService poolQuestionService = new PoolQuestionServiceImpl();
	private ChapterService chapterService = new ChapterServiceImpl();
	private SectionService sectionService = new SectionServiceImpl();
	private List<QuestionReport> list = new ArrayList<QuestionReport>();

	public JSONObject copyQuestionContent(Integer sourceBookId, Integer targetBookId) {
		System.out.println("-------Copying question content started-------");
		try {
			List<Integer> questionList = this.questionService.getQuestionIdsByBookId(sourceBookId);
			System.out.println("Source book question count = " + questionList.size());
			int count = 0;
			for (Integer questionId : questionList) {
				QuestionForRest questionForRest = questionService.findParamsRadioByID(questionId);
				if (null == questionForRest) {
					System.out.println("Question details not found, Question Id = " + questionId);
					continue;
				}

				Integer isTutorial = questionForRest.getIsTutorial();
				Integer poolState = questionForRest.getPoolState();

				if (isTutorial == Constants.TUTORIAL_LESSON_QUESTION) {
					System.out.println("----- Processing Tutorial question ----- , Question Id = " + questionId);
					cloneAssignedQuestionAndStepQuestionForTutorial(questionId, sourceBookId, targetBookId);
				} else if (poolState == Constants.IS_POOL_QUESTION) {
					System.out.println("----- Processing Pool question ----- , Question Id = " + questionId);
					clonePoolQuestion(questionId, sourceBookId, targetBookId);
				} else {
					System.out.println("----- Processing question ----- , Question Id = " + questionId);
					JSONObject jsonObject = duplicateQuestion(questionId, sourceBookId, targetBookId, false, false,
							false, -1, 0);
					this.createQuestionReport(jsonObject);
				}
				count++;
				if (count % 100 == 0) {
					try {
						Thread.sleep(1000);
						HibernateUtil.getCurrentSession().clear();
					} catch (Exception e) {
						System.err.println("Error : " + e);
					}

				}
				System.out.println("Count= " + count);
			}
		} catch (Exception e) {
			System.err.println("Error Occurred. " + e);
		}

		QuestionReportManager reportManager = new QuestionReportManager();
		String fileName = "Question_Report_" + (new Date()).getTime() + ".csv";
		try {
			reportManager.writeToFile(fileName, list);
		} catch (Exception e) {
			System.err.println("Failed to create a report file. " + e);
		}

		return null;
	}

	private void cloneAssignedQuestionAndStepQuestionForTutorial(Integer questionId, Integer sourceBookId,
			Integer targetBookId) {

		JSONObject assignedQuestionJsonObject = new JSONObject();

		boolean isTemplate = false;
		boolean isTutorial = true;
		boolean isStepQuestion = false;
		int stepQuestionCount = -1;
		int assignedQuestionID = 0;

		assignedQuestionJsonObject = duplicateQuestion(questionId, sourceBookId, targetBookId, isTemplate, isTutorial,
				isStepQuestion, stepQuestionCount, assignedQuestionID);
		boolean allSaved = true;

		if (assignedQuestionJsonObject.getInt("questionID") != 0) {
			assignedQuestionID = assignedQuestionJsonObject.getInt("questionID");

			List<Integer> stepQuestionIds = tutorialQuestionService.getStepQuestionIDsByParentID(questionId);
			try {
				isTutorial = false;
				isStepQuestion = true;
				for (int i = 0; i < stepQuestionIds.size(); i++) {
					int stepQuestionId = stepQuestionIds.get(i);
					int currentStepQuestionCount = stepQuestionCount + (i + 2);
					if (currentStepQuestionCount > Constants.STEP_QUESTION_COUNT) {
						break;
					} else {
						System.out.println(
								"----- Processing Tutorial Step question ----- , Step Question Id = " + stepQuestionId);
						JSONObject jsonObjectClone = duplicateQuestion(stepQuestionId, sourceBookId, targetBookId,
								isTemplate, isTutorial, isStepQuestion, currentStepQuestionCount, assignedQuestionID);
						if (jsonObjectClone.getInt("questionID") != 0) {
							int oldStepQuestionId = stepQuestionId;
							int newStepQuestionId = jsonObjectClone.getInt("questionID");

							tutorialQuestionService.getTutQuesionByStepQID(newStepQuestionId).setStepText(
									tutorialQuestionService.getTutQuesionByStepQID(oldStepQuestionId).getStepText());
						} else {
							allSaved = false;
						}
						this.createQuestionReport(jsonObjectClone);
					}
				}
			} catch (Exception e) {
				System.err.println("Error Occurred. " + e);
				assignedQuestionJsonObject.remove("status");
				assignedQuestionJsonObject.put("status", "Failed");
				String comments = assignedQuestionJsonObject.getString("comments")
						+ ", Failed to save tutorial step questions.";
				assignedQuestionJsonObject.remove("comments");
				assignedQuestionJsonObject.put("comments", comments);
			}
		}
		if (!allSaved) {
			assignedQuestionJsonObject.remove("status");
			assignedQuestionJsonObject.put("status", "Failed");
			String comments = assignedQuestionJsonObject.getString("comments") + ", Failed to save tutorial questions.";
			assignedQuestionJsonObject.remove("comments");
			assignedQuestionJsonObject.put("comments", comments);
		}
		this.createQuestionReport(assignedQuestionJsonObject);
	}

	private void clonePoolQuestion(Integer questionId, Integer sourceBookId, Integer targetBookId) {
		boolean isTemplate = false;
		boolean isTutorial = false;
		boolean isStepQuestion = false;
		int stepQuestionCount = -1;
		int poolId = 0;

		JSONObject resultJsonObject = duplicateQuestion(questionId, sourceBookId, targetBookId, isTemplate, isTutorial,
				isStepQuestion, stepQuestionCount, poolId);

		if (resultJsonObject.getInt("questionID") != 0) {
			try {
				poolId = resultJsonObject.getInt("questionID");

				List<PoolQuestion> validPoolQues = this.poolQuestionService
						.getValidPoolQuesByPoolIdWithOrder(questionId);
				System.out.println("Child questions count = " + validPoolQues.size());
				for (int i = 0; i < validPoolQues.size(); i++) {
					PoolQuestion pQue = new PoolQuestion();
					pQue.setPoolId(poolId);
					pQue.setQuestionId(validPoolQues.get(i).getQuestionId());
					pQue.setQuestionIndex(i + 1);
					pQue.setCreateTime(new Date());
					pQue.setUpdateTime(new Date());
					System.out.println("Copying child question = " + validPoolQues.get(i).getQuestionId());
					this.poolQuestionService.savePoolQue(pQue);
				}
			} catch (Exception e) {
				System.err.println("Error Occurred. " + e);
				resultJsonObject.remove("status");
				resultJsonObject.put("status", "Failed");
				String comments = resultJsonObject.getString("comments") + ", Failed to save pool questions.";
				resultJsonObject.remove("comments");
				resultJsonObject.put("comments", comments);
			}
		}
		this.createQuestionReport(resultJsonObject);
	}

	private JSONObject duplicateQuestion(int questionId, int sourceBookId, int targetBookId, boolean isTemplate,
			boolean isTutorial, boolean isStepQuestion, int currentStepQuestionCount, int assignedQuestionId) {
		JSONObject jsonObject = new JSONObject();
		JSONObject stepQuestionJsonObject = new JSONObject();

		try {
			QuestionForRest questionForRest = questionService.findParamsRadioByID(questionId);
			QuestionForRest cloneQuestionForRest;
			Question questionForLog = null;
			try {
				cloneQuestionForRest = (QuestionForRest) questionForRest.deepCopy();
				try {
					cloneQuestionForRest.setQuestionID(null);
					cloneQuestionForRest.setCreateTime(new Date());
					cloneQuestionForRest.setLatestTime(new Date());
					cloneQuestionForRest.setDemoteNote("");
					cloneQuestionForRest.setLiveEditNote("");
					cloneQuestionForRest.setQuestionState(Constants.QUESTION_STATE_INPROGRESS);
					cloneQuestionForRest.setQuestionFrom(questionForRest.getQuestionID());
					cloneQuestionForRest.setQuestionDescription(null);
					cloneQuestionForRest.setIsDemoQuestion(Constants.NOT_IS_DEMO_QUESTION);
					cloneQuestionForRest.setQuestionTitle(questionForRest.getQuestionTitle());
					cloneQuestionForRest.setQuestionVersion(1);
					BookAuthor bookAuthor = this.bookAuthorService.findBookAuthor(questionForRest.getAuthor(),
							sourceBookId);
					if (null != bookAuthor) {
						cloneQuestionForRest.setAuthor(questionForRest.getAuthor());
					}
					cloneQuestionForRest.setQuestionScope(Constants.QUESTION_SCOPE_AMS);
					cloneQuestionForRest.setDisplayAuthor(Constants.DISPLAY_AUTHOR_AMS);

					List<Widget> copiedWidgets = new ArrayList<Widget>();
					List<JSONObject> copiedWidgetIDPairs = this.cloneWidgetsByQuestionID(questionId, copiedWidgets);

					String cloneIntroduction = cloneQuestionForRest.getIntroductionText();
					String cloneQuestionSolution = cloneQuestionForRest.getSolutionText();

					cloneIntroduction = this.replaceWdigetID(copiedWidgetIDPairs, cloneIntroduction);
					cloneQuestionSolution = this.replaceWdigetID(copiedWidgetIDPairs, cloneQuestionSolution);

					cloneQuestionForRest.setIntroductionText(cloneIntroduction);
					cloneQuestionForRest.setSolutionText(cloneQuestionSolution);

					Pattern pattern = Pattern.compile(
							"wafer_ps_(3dmolecule|graph2|select|radio|checkbox|number|shortanswer|graph|labeling|ranking|sorting|chemi|mechanism)_\\w*\\d{10}");
					System.out.println("Question Part Count = " + cloneQuestionForRest.getParts().size());
					for (Part clonePart : cloneQuestionForRest.getParts()) {
						System.out.println("Copying part Id = " + clonePart.getPartID());
						clonePart.setPartID(null);
						clonePart.setQuestion(cloneQuestionForRest);
						String clonePartText = clonePart.getText();
						String clonePartContent = clonePart.getContent();
						String clonePartHint = clonePart.getHintText();
						String clonePartSolution = clonePart.getSolutionText();

						clonePartHint = this.replaceWdigetID(copiedWidgetIDPairs, clonePartHint);
						clonePartSolution = this.replaceWdigetID(copiedWidgetIDPairs, clonePartSolution);
						clonePartText = this.replaceWdigetID(copiedWidgetIDPairs, clonePartText);
						clonePartContent = this.replaceWdigetID(copiedWidgetIDPairs, clonePartContent);
						System.out.println("Parts Components Count = " + clonePart.getComponents().size());
						for (Components component : clonePart.getComponents()) {
							String compId = component.getComponentID();
							System.out.println("Copying component = " + compId);
							String currentCloneCompID = "";
							if (component.getComponentType() == Constants.COMPONENT_TYPE_CHEMISTRY
									|| component.getComponentType() == Constants.COMPONENT_TYPE_GRAPH_V2
									|| component.getComponentType() == Constants.COMPONENT_TYPE_3D_MOLECULE
									|| component.getComponentType() == Constants.COMPONENT_TYPE_MECHANISM) {
								for (int i = 0; i < copiedWidgetIDPairs.size(); i++) {
									JSONObject widgetIDPair = copiedWidgetIDPairs.get(i);
									if (compId.equals(widgetIDPair.getString("oldID"))) {
										currentCloneCompID = widgetIDPair.getString("newID");
										break;
									}
								}
							} else {
								int endIndex = String.valueOf(new Date().getTime()).length();
								int startIndex = endIndex - 10;
								currentCloneCompID = compId.substring(0, compId.indexOf("_div") + 4)
										+ (String.valueOf(new Date().getTime() + 1)).substring(startIndex, endIndex)
										+ compId.substring(compId.indexOf("_div") + 14);
							}
							String componentData = component.getComponentDatas();
							componentData = this.replaceWdigetID(copiedWidgetIDPairs, componentData);
							component.setComponentDatas(componentData);
							String componentDesign = component.getComponentDesign();
							componentDesign = this.replaceWdigetID(copiedWidgetIDPairs, componentDesign);
							component.setComponentDesign(componentDesign);

							clonePartText = clonePartText.replaceAll(compId, currentCloneCompID);
							clonePartContent = clonePartContent.replaceAll(compId, currentCloneCompID);

							component.setComponentID(currentCloneCompID);
							component.setPart(clonePart);

							if (component.getComponentType() == Constants.COMPONENT_TYPE_GRAPH) {
								Graph graph = graphService.findGraphByImgID(compId);
								if (null != graph) {
									Graph cloneGraph = (Graph) graph.deepCopy();
									cloneGraph.setGraphID(null);
									cloneGraph.setGraph_img_ID(component.getComponentID());
									graphService.saveGraphForObject(cloneGraph);
								}
							}
						}
						Matcher matcher = pattern.matcher(clonePartText);
						Date crrtDate = new Date();
						int endIndex = String.valueOf(crrtDate.getTime()).length();
						int startIndex = endIndex - 10;
						String tempDate = (String.valueOf(crrtDate.getTime())).substring(startIndex, endIndex);
						while (matcher.find()) {
							if (matcher.group().indexOf("_div") == -1) {
								tempDate = String.valueOf(Integer.valueOf(tempDate) + 1);
								clonePartText = clonePartText.replaceAll(matcher.group(),
										matcher.group().substring(0, matcher.group().length() - 10) + tempDate);
							}
						}
						matcher = pattern.matcher(clonePartContent);
						while (matcher.find()) {
							if (matcher.group().indexOf("_div") == -1) {
								tempDate = String.valueOf(Integer.valueOf(tempDate) + 1);
								clonePartContent = clonePartContent.replaceAll(matcher.group(),
										matcher.group().substring(0, matcher.group().length() - 10) + tempDate);
							}
						}

						clonePart.setHintText(clonePartHint);
						clonePart.setSolutionText(clonePartSolution);
						clonePart.setText(clonePartText);
						clonePart.setContent(clonePartContent);
					}

					for (Parameter parameter : cloneQuestionForRest.getParameters()) {
						parameter.setParameterID(null);
						parameter.setQuestion(cloneQuestionForRest);
					}

					if (isTutorial) {
						cloneQuestionForRest.setIsTutorial(Constants.TUTORIAL_LESSON_QUESTION);
					} else if (isStepQuestion) {
						cloneQuestionForRest.setIsTutorial(Constants.TUTORIAL_STEP_QUESTION);
					} else {
						cloneQuestionForRest.setIsTutorial(Constants.ORDINARY_STANDALONE_QUESTION);
					}
					System.out.println("Inserting question record, Question Id = " + questionId);
					Question question1 = questionService.saveQuestionForRest(cloneQuestionForRest);
					if (null != question1) {
						question1.setItemID(question1.getQuestionID());

						this.questionService.update(question1);
						TutorialQuestion tq = new TutorialQuestion();
						if (isStepQuestion) { 
							Integer newQuestionID = question1.getQuestionID();
							Date date = new Date();
							TutorialQuestion tutorialQuestion = new TutorialQuestion();
							if (assignedQuestionId > 0) {
								tutorialQuestion.setQuestionID(assignedQuestionId);
							}
							tutorialQuestion.setStepQuestionID(newQuestionID);
							tutorialQuestion.setStepIndex(currentStepQuestionCount);
							tutorialQuestion.setStepText("");
							tutorialQuestion.setCreateTime(date);
							tutorialQuestion.setUpdateTime(date);
							tq = this.tutorialQuestionService.saveTutorialQuestion(tutorialQuestion);
						}

						System.out.println("Questions widget count = " + copiedWidgets.size());
						for (Widget widget : copiedWidgets) {
							widget.setQuestionID(question1.getQuestionID());
							widget.setCreateTime(new Date());
							System.out.println("Updating widget.....!!!");
							this.widgetService.saveOrUpdateWidget(widget);
						}

						questionForLog = question1;
						jsonObject.put("questionID", questionForLog.getQuestionID());
						if (isStepQuestion) {
							stepQuestionJsonObject.put("questionID", question1.getQuestionID());
							if (null != tq) {
								stepQuestionJsonObject.put("index", tq.getStepIndex());
							}
							jsonObject = stepQuestionJsonObject;
						}
					} else {
						System.err.println("Error occurred while copying the content. ");
						if (!jsonObject.containsKey("questionID")) {
							jsonObject.put("questionID", 0);
						}
						jsonObject.put("oldQuestionId", questionId);
						jsonObject.put("status", "Failed");
						jsonObject.put("comments", "Failed to insert record into ps_question table.");
						// throw new RuntimeException(e);
						return jsonObject;
					}
				} catch (Exception e) {
					HibernateUtil.closeSession();
					System.err.println("Error occurred while copying the content. " + e);
					if (!jsonObject.containsKey("questionID")) {
						jsonObject.put("questionID", 0);
					}
					jsonObject.put("oldQuestionId", questionId);
					jsonObject.put("status", "Failed");
					jsonObject.put("comments", "Failed to insert record into ps_question table.");
					// throw new RuntimeException(e);
					return jsonObject;
				}
				try {
					QuestionBook questionBook = questionService.getQuestionBook(questionId, sourceBookId);
					QuestionBook cloneQuestionBook = (QuestionBook) questionBook.deepCopy();
					cloneQuestionBook.setQuestionBookID(null);
					cloneQuestionBook.setBookID(targetBookId);
					cloneQuestionBook.setQuestionID(questionForLog.getQuestionID());
					cloneQuestionBook.setQuestionNumber(questionBook.getQuestionNumber());
					cloneQuestionBook.setDifficlutyRating(questionBook.getDifficlutyRating());

					Chapter sourceBookChapter = this.chapterService.findChapter(cloneQuestionBook.getChapterID());
					Integer sourceChapterOrderIndex = null != sourceBookChapter ? sourceBookChapter.getOrderIndex() : 0;
					Integer targetBookChapterOrderIndex = ChapterUtil.getMappedChapterNumber(sourceChapterOrderIndex);
					Chapter targetBookChapter = this.chapterService.findChapter(targetBookId,
							targetBookChapterOrderIndex);
					int chapterId = (null != targetBookChapter) ? targetBookChapter.getChapterID() : 0;
					if (chapterId != 0) {
						Section section = this.sectionService.getSectionsByChapterID(chapterId);
						int sectionId = null != section ? section.getSectionID() : 0;
						System.out.println("Chapter Id : " + chapterId + " , Section Id : " + sectionId);
						cloneQuestionBook.setSectionID(sectionId);
						cloneQuestionBook.setChapterID(chapterId);
					}
					if (isTutorial || isStepQuestion) {
						int series = this.checkSeriesForTut(sourceBookId, targetBookId);
						cloneQuestionBook.setSeries(series);
					}

					cloneQuestionBook.setMappingStatus(Constants.MAPPING_STATE_CHECKED);
					cloneQuestionBook.setMappingOrigin(Constants.MAPPING_ORIGIN_NOT);
					System.out.println("Creating a mapping record for question in question book table, Question Id = "
							+ questionId);
					questionService.saveQuestionBook(cloneQuestionBook);

					System.out.println("Copying learning objective of question " + questionId);
					questionService.cloneObjectives(questionBook.getQuestionBookID(),
							cloneQuestionBook.getQuestionBookID());

					System.out.println("Copying learning outcomes of question " + questionId);
					questionService.cloneOutComes(questionBook.getQuestionBookID(),
							cloneQuestionBook.getQuestionBookID(), cloneQuestionForRest.getQuestionID());

					if (!isStepQuestion) {
						jsonObject.put("sectionID", cloneQuestionBook.getSectionID());
					}
				} catch (Exception e) {
					HibernateUtil.closeSession();
					System.err.println("Error occurred while copying the content. " + e);
					if (!jsonObject.containsKey("questionID")) {
						jsonObject.put("questionID", 0);
					}
					jsonObject.put("oldQuestionId", questionId);
					jsonObject.put("status", "Failed");
					jsonObject.put("comments", "Failed to insert record into ps_question_book table.");

					return jsonObject;
				}
			} catch (Exception e) {
				HibernateUtil.closeSession();
				System.err.println("Error occurred while copying the content. " + e);
				if (!jsonObject.containsKey("questionID")) {
					jsonObject.put("questionID", 0);
				}
				jsonObject.put("oldQuestionId", questionId);
				jsonObject.put("status", "Failed");
				jsonObject.put("comments", "Failed to create record for question.");

				return jsonObject;
			}
		} catch (Exception e) {
			HibernateUtil.closeSession();
			System.err.println("Error occurred while copying the content. " + e);
			if (!jsonObject.containsKey("questionID")) {
				jsonObject.put("questionID", 0);
			}
			jsonObject.put("oldQuestionId", questionId);
			jsonObject.put("status", "Failed");
			jsonObject.put("comments", "Failed to create record for question.");

			return jsonObject;
		}
		if (!jsonObject.containsKey("questionID")) {
			jsonObject.put("questionID", 0);
		}
		jsonObject.put("oldQuestionId", questionId);
		jsonObject.put("status", "Success");
		jsonObject.put("comments", "");

		return jsonObject;
	}

	private List<JSONObject> cloneWidgetsByQuestionID(int questionID, List<Widget> copiedWidgets) throws Exception {
		List<JSONObject> copiedWidgetIDPairs = new ArrayList<JSONObject>();// Store the id mapping relationship between
																			// the old and new wiget
		List<Widget> allQuestionWidgets = widgetService.getWidgetByQuestionId(questionID);
		for (Widget oldWidget : allQuestionWidgets) {
			Widget newWidget = (Widget) oldWidget.deepCopy();
			newWidget.setWidgetId(null);
			String oldWidgetDivID = oldWidget.getWidgetDivId();
			int endIndex = String.valueOf(new Date().getTime()).length();
			int startIndex = endIndex - 10;
			String newWidgetDivID = oldWidgetDivID.substring(0, oldWidgetDivID.indexOf("_div") + 4)
					+ (String.valueOf(new Date().getTime() + 1)).substring(startIndex, endIndex)
					+ oldWidgetDivID.substring(oldWidgetDivID.indexOf("_div") + 14);
			newWidget.setWidgetDivId(newWidgetDivID);
			if (oldWidget.getWidgetType() == Constants.COMPONENT_TYPE_CHEMISTRY
					|| Constants.COMPONENT_TYPE_MECHANISM == oldWidget.getWidgetType()) {
				CompressDataAndDecompressData cd = new CompressDataAndDecompressData();
				String tempDataStr = cd.decompressData(newWidget.getWidgetData());
				String newDataStr = "";
				tempDataStr = tempDataStr.replaceAll(oldWidgetDivID, newWidgetDivID);
				newDataStr = cd.compressData(tempDataStr);
				newWidget.setWidgetData(newDataStr);
			}
			JSONObject json = new JSONObject();
			json.put("oldID", oldWidgetDivID);
			json.put("newID", newWidgetDivID);
			copiedWidgetIDPairs.add(json);
			copiedWidgets.add(newWidget);
		}

		return copiedWidgetIDPairs;
	}

	private String replaceWdigetID(List<JSONObject> copiedWidgetIDPairs, String context) throws Exception {
		for (int i = 0; i < copiedWidgetIDPairs.size(); i++) {
			JSONObject jsObj = copiedWidgetIDPairs.get(i);
			String newWidgetID = jsObj.getString("newID");
			String oldWidgetID = jsObj.getString("oldID");
			context = context.replaceAll(oldWidgetID, newWidgetID);
		}
		return context;
	}

	public int checkSeriesForTut(int bookID, int targetBookId) throws Exception {
		int tutSeriesID = bookSeriesService.getBookSerise(bookID, "TUT");
		if (tutSeriesID == 0) {
			BookSeries bs = new BookSeries();
			bs.setBookID(targetBookId);
			bs.setSeries("TUT");
			bs.setDescriptiveName("Tutorial Lesson");
			bs.setIsApplySeriesFilter(1);
			try {
				bookSeriesService.saveBookSeries(bs);
			} catch (Exception e) {
				System.out.println(e.getMessage() + ", " + e);
			}
		}
		tutSeriesID = bookSeriesService.getBookSerise(targetBookId, "TUT");

		return tutSeriesID;
	}

	private void createQuestionReport(JSONObject jsonObject) {
		QuestionReport questionReport = new QuestionReport();
		questionReport.setOldQuestionId(jsonObject.getInt("oldQuestionId"));
		questionReport.setNewQuestionId(jsonObject.getInt("questionID"));
		questionReport.setStatus(jsonObject.getString("status"));
		questionReport.setComments(jsonObject.getString("comments"));

		list.add(questionReport);
	}

}
