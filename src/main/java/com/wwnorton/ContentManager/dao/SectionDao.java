package com.wwnorton.ContentManager.dao;

import com.wwnorton.ContentManager.entity.Section;

public interface SectionDao {

	Section getSectionsByChapterID(int chapterID);

}
