package com.wwnorton.ContentManager.dao;

import java.util.List;

import com.wwnorton.ContentManager.entity.PoolQuestion;

public interface PoolQuestionDao {

	List<PoolQuestion> getValidPoolQuesByPoolIdWithOrder(int poolId);

	PoolQuestion savePoolQue(PoolQuestion poolQue);

}
