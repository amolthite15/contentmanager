package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.wwnorton.ContentManager.entity.PoolQuestion;
import com.wwnorton.ContentManager.util.Constants;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class PoolQuestionDaoImpl implements PoolQuestionDao {

	public List<PoolQuestion> getValidPoolQuesByPoolIdWithOrder(int poolId) {
		Session session = HibernateUtil.getCurrentSession();
		String hql = "select pq from PoolQuestion pq, Question q where pq.questionId = q.questionID and pq.poolId = :poolid and q.questionState <> :questionstate order by pq.questionIndex asc";
		Query query = session.createQuery(hql);
		query.setParameter("poolid", poolId);
		query.setParameter("questionstate", Constants.QUESTION_STATE_DEMOTED);

		return query.list();
	}

	public PoolQuestion savePoolQue(PoolQuestion poolQue) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(poolQue);
		session.flush();
		transaction.commit();

		return poolQue;
	}

}
