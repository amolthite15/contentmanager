package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.wwnorton.ContentManager.entity.Graph;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class GraphDaoImpl implements GraphDao {

	public Graph findGraphByImgID(String id) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer("select * from ps.ps_graph where graph_img_ID= :graphimgid");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("graphimgid", id);
		query.addEntity(Graph.class);
		List<Graph> list = query.list();
		if (null != list && list.size() > 0) {
			return (Graph) list.get(0);
		}

		return null;
	}

	public void saveGraphForObject(Graph graph) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(graph);
		session.flush();
		transaction.commit();
	}
}
