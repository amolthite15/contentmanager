package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.wwnorton.ContentManager.entity.TutorialQuestion;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class TutorialQuestionDaoImpl implements TutorialQuestionDao {

	public TutorialQuestion saveTutorialQuestion(TutorialQuestion tutorialQuestion) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(tutorialQuestion);
		session.flush();
		transaction.commit();

		return tutorialQuestion;
	}

	public List<Integer> getStepQuestionIDsByParentID(Integer questionID) {
		Session session = HibernateUtil.getCurrentSession();
		String hql = "select tq.stepQuestionID from TutorialQuestion tq where tq.questionID = :questionid";
		Query query = session.createQuery(hql);
		query.setParameter("questionid", questionID);
		
		return query.list();
	}

	public TutorialQuestion getTutQuesionByStepQID(int stepQID) {
		Session session = HibernateUtil.getCurrentSession();
		String hql = "from TutorialQuestion tq where tq.stepQuestionID = :stepquestionid";
		Query query = session.createQuery(hql);
		query.setParameter("stepquestionid", stepQID);
		
		return (TutorialQuestion) query.uniqueResult();
	}
}
