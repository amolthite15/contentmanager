package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.wwnorton.ContentManager.entity.BookSeries;
import com.wwnorton.ContentManager.entity.QuestionForRest;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class BookSeriesDaoImpl implements BookSeriesDao {

	public int getBookSerise(int bookID, String serise) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer(
				"select * from ps.ps_book_series where Book_ID= :bookid and Series= :series");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("bookid", bookID);
		query.setParameter("series", serise);
		query.addEntity(BookSeries.class);
		List<BookSeries> list = query.list();
		if (null != list && list.size() > 0) {
			BookSeries bookSeries = (BookSeries) list.get(0);
			return bookSeries.getBookSeriesID();
		}
		return 0;
	}

	public void saveBookSeries(BookSeries bookSeries) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(bookSeries);
		session.flush();
		transaction.commit();
	}

}
