package com.wwnorton.ContentManager.dao;

import com.wwnorton.ContentManager.entity.QuestionBook;

public interface QuestionBookDao {

	QuestionBook getQuestionBook(int questionId, int bookId);

}
