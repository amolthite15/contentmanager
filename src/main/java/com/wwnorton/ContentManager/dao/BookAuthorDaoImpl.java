package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.wwnorton.ContentManager.entity.BookAuthor;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class BookAuthorDaoImpl implements BookAuthorDao {

	public BookAuthor findBookAuthor(String userId, int bookId) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer(
				"select * from ps.ps_book_author where Book_ID= :bookid and Author_ID= :authorid");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("bookid", bookId);
		query.setParameter("authorid", userId);
		query.addEntity(BookAuthor.class);
		List<BookAuthor> list = query.list();
		if (null != list && list.size() > 0) {
			System.out.println("Query List" + list.get(0).toString());
			return (BookAuthor) list.get(0);
		}
		
		return null;
	}

}
