package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.wwnorton.ContentManager.entity.QuestionBook;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class QuestionBookDaoImpl implements QuestionBookDao {

	public QuestionBook getQuestionBook(int questionId, int bookId) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer(
				"select * from ps.ps_question_book where Question_ID= :questionid and Book_ID= :bookid");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("questionid", questionId);
		query.setParameter("bookid", bookId);
		query.addEntity(QuestionBook.class);
		List<QuestionBook> list = query.list();
		if (null != list && list.size() > 0) {
			System.out.println("Query List" + list.get(0).toString());
			return (QuestionBook) list.get(0);
		}

		return null;
	}

}
