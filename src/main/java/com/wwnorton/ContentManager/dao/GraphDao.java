package com.wwnorton.ContentManager.dao;

import com.wwnorton.ContentManager.entity.Graph;

public interface GraphDao {

	Graph findGraphByImgID(String id);

	void saveGraphForObject(Graph graph);

}
