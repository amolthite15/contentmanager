package com.wwnorton.ContentManager.dao;

import com.wwnorton.ContentManager.entity.BookAuthor;

public interface BookAuthorDao {

	BookAuthor findBookAuthor(String userId, int bookId);

}
