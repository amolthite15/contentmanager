package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.wwnorton.ContentManager.entity.Question;
import com.wwnorton.ContentManager.entity.QuestionBook;
import com.wwnorton.ContentManager.entity.QuestionForRest;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class QuestionDaoImpl implements QuestionDao {

	@SuppressWarnings("unchecked")
	public List<Integer> getQuestionIdsByBookId(Integer bookId) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer(
				"select q.Question_Id from ps.ps_question q, ps.ps_question_book qb where q.Question_Id=qb.Question_Id and q.Question_Scope=0 and qb.Book_Id= :bookid");

		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("bookid", bookId);

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public QuestionForRest findParamsRadioByID(Integer id) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer("select * from ps.ps_question where Question_ID= :questionid");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("questionid", id);
		query.addEntity(QuestionForRest.class);

		List<QuestionForRest> list = query.list();
		if (null != list && list.size() > 0) {
			return (QuestionForRest) list.get(0);
		}

		return null;
	}

	public Question saveQuestionForRest(Question question) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(question);
		session.flush();
		transaction.commit();

		return question;
	}

	public void update(Question question) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.update(question);
		session.flush();
		transaction.commit();
	}

	@SuppressWarnings("unchecked")
	public QuestionBook getQuestionBook(int questionId, int bookId) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer(
				"select * from ps.ps_question_book where Question_ID= :questionid and Book_ID= :bookid");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("questionid", questionId);
		query.setParameter("bookid", bookId);
		query.addEntity(QuestionBook.class);
		List<QuestionBook> list = query.list();
		if (null != list && list.size() > 0) {
			return (QuestionBook) list.get(0);
		}

		return null;
	}

	public void saveQuestionBook(QuestionBook questionBook) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(questionBook);
		session.flush();
		transaction.commit();
	}

	public void cloneObjectives(Integer sourceQuestionBookID, Integer targetQuestionBookID) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		String hql = "Insert into QuestionLearningObjective(questionBookID, objectiveID) Select " + targetQuestionBookID
				+ ", ql.objectiveID From QuestionLearningObjective ql where ql.questionBookID= :questionbookid";
		Query query = session.createQuery(hql);
		query.setParameter("questionbookid", sourceQuestionBookID);
		query.executeUpdate();
		session.flush();
		transaction.commit();
	}

	public void cloneOutComes(Integer sourceQuestionBookID, Integer targetQuestionBookID, Integer targetQuestionId) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		StringBuffer hql = new StringBuffer();
		hql.append("INSERT INTO ");
		hql.append(
				"QuestionLearningOutcomes(questionBookId,learningOutcomeId,bookId,questionId,learningOutComeSetId) ");
		hql.append("SELECT ").append(targetQuestionBookID).append(",qlo.learningOutcomeId,qlo.bookId,")
				.append(targetQuestionId).append(",qlo.learningOutComeSetId ");
		hql.append("FROM QuestionLearningOutcomes qlo WHERE qlo.questionBookId = :questionbookid");

		Query query = session.createQuery(hql.toString());
		query.setParameter("questionbookid", sourceQuestionBookID);

		query.executeUpdate();
		session.flush();
		transaction.commit();
	}

}
