package com.wwnorton.ContentManager.dao;

import com.wwnorton.ContentManager.entity.Chapter;

public interface ChapterDao {

	Chapter findChapter(int chapterID);

	Chapter findChapter(int bookId, int orderIndex);

}
