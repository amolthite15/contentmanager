package com.wwnorton.ContentManager.dao;

import java.util.List;

import com.wwnorton.ContentManager.entity.Widget;

public interface WidgetDao {
	void saveOrUpdateWidget(Widget widget);

	List<Widget> getWidgetByQuestionId(int questionId);
}
