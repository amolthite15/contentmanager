package com.wwnorton.ContentManager.dao;

import java.util.List;

import com.wwnorton.ContentManager.entity.Question;
import com.wwnorton.ContentManager.entity.QuestionBook;
import com.wwnorton.ContentManager.entity.QuestionForRest;

public interface QuestionDao {

	List<Integer> getQuestionIdsByBookId(Integer bookId);

	QuestionForRest findParamsRadioByID(Integer id);

	Question saveQuestionForRest(Question question);

	void update(Question question);

	QuestionBook getQuestionBook(int questionId, int bookId);

	void saveQuestionBook(QuestionBook questionBook);

	void cloneObjectives(Integer sourceQuestionBookID, Integer targetQuestionBookID);

	void cloneOutComes(Integer sourceQuestionBookID, Integer targetQuestionBookID, Integer targetQuestionId);

}
