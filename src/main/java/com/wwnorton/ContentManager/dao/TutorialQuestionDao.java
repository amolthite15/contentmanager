package com.wwnorton.ContentManager.dao;

import java.util.List;

import com.wwnorton.ContentManager.entity.TutorialQuestion;

public interface TutorialQuestionDao {

	TutorialQuestion saveTutorialQuestion(TutorialQuestion tutorialQuestion);

	List<Integer> getStepQuestionIDsByParentID(Integer questionID);

	TutorialQuestion getTutQuesionByStepQID(int stepQID);

}
