package com.wwnorton.ContentManager.dao;

import com.wwnorton.ContentManager.entity.BookSeries;

public interface BookSeriesDao {

	int getBookSerise(int bookID, String serise);

	void saveBookSeries(BookSeries bookSeries);

}
