package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.wwnorton.ContentManager.entity.Widget;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class WidgetDaoImpl implements WidgetDao {

	public void saveOrUpdateWidget(Widget widget) {
		Session session = HibernateUtil.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(widget);
		session.flush();
		transaction.commit();
	}

	public List<Widget> getWidgetByQuestionId(int questionId) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer("select * from ps.ps_widget where QuestionId= :questionid");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("questionid", questionId);
		query.addEntity(Widget.class);

		return query.list();
	}
}
