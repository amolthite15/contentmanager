package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.wwnorton.ContentManager.entity.Chapter;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class ChapterDaoImpl implements ChapterDao {

	public Chapter findChapter(int chapterID) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer("select * from ps.ps_chapter where Chapter_ID= :chapterid");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("chapterid", chapterID);
		query.addEntity(Chapter.class);
		List<Chapter> list = query.list();
		if (null != list && list.size() > 0) {
			return (Chapter) list.get(0);
		}
		return null;
	}

	public Chapter findChapter(int bookId, int orderIndex) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer(
				"select * from ps.ps_chapter where Book_Id= :bookid and Order_Index= :orderindex");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("bookid", bookId);
		query.setParameter("orderindex", orderIndex);
		query.addEntity(Chapter.class);
		List<Chapter> list = query.list();
		if (null != list && list.size() > 0) {
			return (Chapter) list.get(0);
		}
		return null;
	}

}
