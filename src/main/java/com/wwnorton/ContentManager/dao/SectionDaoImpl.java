package com.wwnorton.ContentManager.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.wwnorton.ContentManager.entity.Section;
import com.wwnorton.ContentManager.util.HibernateUtil;

public class SectionDaoImpl implements SectionDao {

	public Section getSectionsByChapterID(int chapterID) {
		Session session = HibernateUtil.getCurrentSession();
		StringBuffer sql = new StringBuffer("select * from ps.ps_section where Chapter_ID= :chapterid");
		SQLQuery query = session.createSQLQuery(sql.toString());
		query.setParameter("chapterid", chapterID);
		query.addEntity(Section.class);
		List<Section> list = query.list();
		if (null != list && list.size() > 0) {
			return (Section) list.get(0);
		}
		return null;
	}

}
