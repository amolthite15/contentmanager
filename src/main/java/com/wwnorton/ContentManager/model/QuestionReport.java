package com.wwnorton.ContentManager.model;

public class QuestionReport {

	private Integer oldQuestionId;
	private Integer newQuestionId;
	private String status;
	private String comments;

	public Integer getOldQuestionId() {
		return oldQuestionId;
	}

	public void setOldQuestionId(Integer oldQuestionId) {
		this.oldQuestionId = oldQuestionId;
	}

	public Integer getNewQuestionId() {
		return newQuestionId;
	}

	public void setNewQuestionId(Integer newQuestionId) {
		this.newQuestionId = newQuestionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
